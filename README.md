<img src="readme/logo.png" align="right" width="100" height="100" />

# Babbeln 
> Free & privacy oriented messenger with unlimited voice and video calls.  
> Licensed under `GNU GPLv3`

<img src="readme/electron.png" href="babbeln-client"/>

<div align="center">[Introduction](#Introduction) • [Install](#install) • [Contribute](#contribute)</div>

## Project Goal(s)

The goal is to make an attractive alternative to 'zoom' or 'discord', but as an easy fork-able project so that you can host your very own server.

## To Do List for v1.0.0

- [x] Voice call functionality 
- [x] Video call functionality
- [x] Timeline with post and images like twitter 
- [ ] Sound & audio input settings/controls
- [ ] Responsive design for mobile devices

## Install

Since `babbeln-client` is still not near production ready, installing or bundling it for personal use is <b>highly unrecommended</b>!  
To get the devlopement environment ready, you might also want to look at the installation instructions of `babbeln-server`.  

## To get the client running follow these steps:

```
git clone https://gitlab.com/marcempunkt/babbeln-client
npm i && npm run electron:dev
```

## NPM scripts 

`npm run <script-name>`

| Script name                    | Description                                                     |
| ------------------------------ | --------------------------------------------------------------- |
| dev                            | Start dev server (only in Browser)                              |
| build                          | Build only website                                              |
| electron:dev                   | Start dev server with electron                                  |
| electron:build                 | Build website & electron app (for windows & linux atm)          |

When bundling babbeln be sure to make `.env` file at the project root with `BABBELN_ENV=production`!

## Contribute

I'm a japanese & korean studies student. I'm not a cs student so therefore any criticism, pull requests or advices are highly appreciated!!!

Don't hestitate to tell me that my code sucks and that I should stick to teaching languages:   
+ <a href="mailto:marc.maeurer@pm.me">Send me an Email</a>

const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = (env) => {

  return {
    mode: (env.production) ? "production" : "development",
    // devtool: "inline-source-map",
    devtool: "source-map",

    // entry point of the application
    entry: path.resolve(__dirname, "./src/index.tsx"),

    // export file
    output: {
      path: path.resolve(__dirname, "build"),
      filename: "bundle.[contenthash].js",
      clean: true, // delete all old files in output.path 
    },

    // dev server is running all the files from memory
    devServer: {
      static: { directory: path.resolve(__dirname, "./src"), },
      // contentBase: path.join(__dirname, "public"),
      port: 3000,
      open: false,
      hot: true,
      compress: true,
      historyApiFallback: true,
      client: {
	overlay: { errors: true, warnings: false },
      },
    },

    // what is resolve?
    resolve: {
      extensions: [".tsx", ".ts", ".js"],
    },

    module: {
      rules: [
	// Typescript
	{
	  test: /\.tsx?$/,
	  use: "ts-loader",
	  exclude: /node_modules/,
	},
	// Sass
	{
	  test: /\.s[ac]ss$/i,
	  use: [
	    // Creates 'style' nodes from JS strings
	    "style-loader",
	    // Translates CSS into CommonJS
	    "css-loader",
	    // add the missing url rewriting
	    "resolve-url-loader",
	    // Compiles Sass to CSS
	    "sass-loader",
	  ],
	},
	// SVGs
	{
	  test: /\.svg$/,
	  // react-svg-loader = import { ReactComponent as X } from "x.svg";
	  // url-loader       = import X from "x.svg";
	  use: [
	    {
	      loader: "@svgr/webpack",
	      options: {
		svgo: false,
	      }
	    },
	    "file-loader"
	  ],
	},
	// Images (could also be used for fonts)
	{
	  test: /\.(png|jpe?g|gif)$/i,
	  use: ["file-loader"],
	},
	// MP3
	{
	  test: /\.mp3$/,
	  use: ["file-loader"],
	},
      ],
    },

    externals: {
      electron: "electron",
    },

    plugins: [
      new HtmlWebpackPlugin({
	filename: "index.html",
	template: "./public/index.html",
      }),
      new Dotenv({
	path: getDotenvPath(env),
      }),
      new ESLintPlugin({
	extensions: [".ts", ".tsx", ".js", ".jsx"],
	exclude: "node_modules",
      }),
      // new webpack.ExternalsPlugin('commonjs', ['electron']),
    ],
  }
};

const getDotenvPath = (env) => {
  switch (env.command) {
    case "dev":
      return path.join("env", "dotenv_dev"); 
    case "build":
      return path.join("env", "dotenv_build"); 
    case "electron:build":
      return path.join("env", "dotenv_electronbuild");
    default:
      return path.join("env", "dotenv_dev");
  }
}

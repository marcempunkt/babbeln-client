import React, { useState, useContext, createContext } from "react";

import { ModalContextValue } from "../ts/types/contextvalue_types";

import DeleteMessageModal from "../components/modals/DeleteMessageModal";
import DeleteChatModal from "../components/modals/DeleteChatModal";
import ChangeProfileImageModal from "../components/modals/ChangeProfileImageModal";
import ChangeUsernameModal from "../components/modals/ChangeUsernameModal";
import ChangeEmailModal from "../components/modals/ChangeEmailModal";
import ChangePasswordModal from "../components/modals/ChangePasswordModal";
import DeleteAccountModal from "../components/modals/DeleteAccountModal";
import RemoveFriendModal from "../components/modals/RemoveFriendModal";
import ShowProfileModal from "../components/modals/ShowProfileModal";
import LogoutModal from "../components/modals/LogoutModal";
import BlockUserModal from "../components/modals/BlockUserModal";
import UnblockUserModal from "../components/modals/UnblockUserModal";
import BlogModal from "../components/modals/BlogModal";
import PremiumModal from "../components/modals/PremiumModal";
import ImageModal from "../components/modals/ImageModal";
import ScreenshareModal from "../components/modals/ScreenshareModal";

import StartCallModal from "../components/modals/StartCallModal";
import ReceiveCallModal from "../components/modals/ReceiveCallModal";

export const ModalContext = createContext<ModalContextValue | undefined>(undefined);

export const useModalContext = () => {
  const context = useContext(ModalContext);
  if (context === undefined) {
    throw Error( "ModalContext is undefined! Are you consuming the ModalContext outside of the ModalContextProvider?" );
  }
  return context;
};

interface Props {
  children: JSX.Element;
}

const ModalContextProvider: React.FC<Props> = (props: Props) => {
  /**
   * State for all the modals
   * if true show modal
   * only one modal at once should be true
   * otherwise bugs will happen
   */

  /*
   * Messages
   * ID as state, so that it can be send to the backend
   */
  const [showReportMessageModal, setShowReportMessageModal] = useState<boolean>(false);
  const [reportMessageId, setReportMessageId] = useState<number>(0);
  const [showDeleteMessageModal, setShowDeleteMessageModal] = useState<boolean>(false);
  const [deleteMessageId, setDeleteMessageId] = useState<number>(0);

  const [showDeleteChatModal, setShowDeleteChatModal] = useState<boolean>(false);
  const [deleteChatFromUserId, setDeleteChatFromUserId] = useState<number>(0);

  /* Change Users XYZ */
  const [showChangeProfileImageModal, setShowChangeProfileImageModal] = useState<boolean>(false);
  const [showChangeUsernameModal, setShowChangeUsernameModal] = useState<boolean>(false);
  const [showChangeEmailModal, setShowChangeEmailModal] = useState<boolean>(false);
  const [showChangePasswordModal, setShowChangePasswordModal] = useState<boolean>(false);
  const [showDeleteAccountModal, setShowDeleteAccountModal] = useState<boolean>(false);

  /* Show Profile of a You or a another User */
  const [showProfileModal, setShowProfileModal] = useState<boolean>(false);
  const [showProfileId, setShowProfileId] = useState<number>(0);

  /* Friends */
  const [showRemoveFriendModal, setShowRemoveFriendModal] = useState<boolean>(false);
  /* removeFriendId is inside FriendsContext */

  /* Logout */
  const [showLogoutModal, setShowLogoutModal] = useState<boolean>(false);

  /* block/unblock friend */
  const [blockOrUnblockUserId, setBlockOrUnblockUserId] = useState<number>(0);
  const [showBlockUserModal, setShowBlockUserModal] = useState<boolean>(false);
  const [showUnblockUserModal, setShowUnblockUserModal] = useState<boolean>(false);

  /* Dashboard */
  const [showBlogModal, setShowBlogModal] = useState<boolean>(false);
  const [showPremiumModal, setShowPremiumModal] = useState<boolean>(false);

  /* Post */
  const [showImageModal, setShowImageModal] = useState<boolean>(false);
  const [imageModalSrc, setImageModalSrc] = useState<string>("");

  /* Call */
  const [showStartCallModal, setShowStartCallModal] = useState<boolean>(false);
  const [showReceiveCallModal, setShowReceiveCallModal] = useState<boolean>(false);
  const [showScreenshareModal, setShowScreenshareModal] = useState<boolean>(false);

  const reset = () => {
    /**
     * Resets all modalcontext state
     * @public
     */
    setShowReportMessageModal(false);
    setReportMessageId(0);
    setShowDeleteMessageModal(false);
    setDeleteMessageId(0);
    setShowDeleteChatModal(false);
    setDeleteChatFromUserId(0);
    setShowChangeProfileImageModal(false);
    setShowChangeUsernameModal(false);
    setShowChangeEmailModal(false);
    setShowChangePasswordModal(false);
    setShowDeleteAccountModal(false);
    setShowProfileModal(false);
    setShowProfileId(0);
    setShowRemoveFriendModal(false);
    setShowLogoutModal(false);
    setBlockOrUnblockUserId(0);
    setShowBlockUserModal(false);
    setShowUnblockUserModal(false);
    setShowBlogModal(false);
    setShowPremiumModal(false);
    setShowImageModal(false);
    setImageModalSrc("");
    setShowStartCallModal(false);
    setShowReceiveCallModal(false);
    setShowScreenshareModal(false);
  };

  const renderModal: () => JSX.Element | null = () => {
    /*
     * Returns the correct (& only one at a time) Modal 
     * that will be rendered inside a render function
     * by the ModalContext
     * TODO turn it into a switch statement with a state var whichModal
     * @public
     *
     * returns {React.FC}     Modal
     */
    if (showDeleteMessageModal) return <DeleteMessageModal />;
    if (showDeleteChatModal) return <DeleteChatModal />;
    if (showChangeProfileImageModal) return <ChangeProfileImageModal />;
    if (showChangeUsernameModal) return <ChangeUsernameModal />;
    if (showChangeEmailModal) return <ChangeEmailModal />;
    if (showChangePasswordModal) return <ChangePasswordModal />;
    if (showDeleteAccountModal) return <DeleteAccountModal />;
    if (showRemoveFriendModal) return <RemoveFriendModal />;
    if (showProfileModal) return <ShowProfileModal />;
    if (showLogoutModal) return <LogoutModal />;
    if (showBlockUserModal) return <BlockUserModal />;
    if (showUnblockUserModal) return <UnblockUserModal />;
    if (showBlogModal) return <BlogModal />;
    if (showPremiumModal) return <PremiumModal />;
    if (showImageModal) return <ImageModal />;
    if (showScreenshareModal) return <ScreenshareModal />;
    return null;
  };

  const renderCallModal: () => JSX.Element | null = () => {
    /*
     * return <CallModal />
     * @public
     *
     * returns  {React.FC}  CallModal
     */
    if (showStartCallModal) return <StartCallModal />;
    if (showReceiveCallModal) return <ReceiveCallModal />;
    return null;
  };

  return(
    <ModalContext.Provider value={{
      /* ~State~ */
      /* ~Message~ */
      showReportMessageModal, setShowReportMessageModal,
      showDeleteMessageModal, setShowDeleteMessageModal,
      reportMessageId, setReportMessageId,
      deleteMessageId, setDeleteMessageId,
      showDeleteChatModal, setShowDeleteChatModal,
      deleteChatFromUserId, setDeleteChatFromUserId,
      /* ~Change User~ */
      showChangeProfileImageModal, setShowChangeProfileImageModal,
      showChangeUsernameModal, setShowChangeUsernameModal,
      showChangeEmailModal, setShowChangeEmailModal,
      showChangePasswordModal, setShowChangePasswordModal,
      showDeleteAccountModal, setShowDeleteAccountModal,
      /* ~Show Profile~ */
      showProfileModal, setShowProfileModal,
      showProfileId, setShowProfileId,
      /* ~Friend~ */
      showRemoveFriendModal, setShowRemoveFriendModal,
      /* ~Log out~ */
      showLogoutModal, setShowLogoutModal,
      blockOrUnblockUserId, setBlockOrUnblockUserId,
      /* ~Block/Unblock friend~ */
      showBlockUserModal, setShowBlockUserModal,
      showUnblockUserModal, setShowUnblockUserModal,
      /* ~Dashboard~ */
      showBlogModal, setShowBlogModal,
      showPremiumModal, setShowPremiumModal,
      /* ~Post~ */
      showImageModal, setShowImageModal,
      imageModalSrc, setImageModalSrc,
      /* ~ Call ~ */
      showStartCallModal, setShowStartCallModal,
      showReceiveCallModal, setShowReceiveCallModal,
      showScreenshareModal, setShowScreenshareModal,
      /* ~Methods~ */
      reset,
      renderModal,
      renderCallModal,
    }}>
      { props.children }
    </ModalContext.Provider>
  );
};

export default ModalContextProvider;

import React, { useEffect, useState, useRef, useContext, createContext } from "react";
import { v4 as uuidv4 } from "uuid";
import logger from "../utils/logger";

import { Layout, CallingType } from "../ts/types/call_types";
import { SocketState } from "../ts/types/socket_types";
import { NotificationType } from "../ts/types/notification_types";
import { useNotificationContext } from "./NotificationContext";
import { useSocketContext } from "./SocketContext";
import { useUserContext } from "./UserContext";
import { useModalContext } from "./ModalContext";
import { CallContextValue,
	 UserContextValue,
	 SocketContextValue,
	 NotificationContextValue,
	 ModalContextValue } from "../ts/types/contextvalue_types";

export const CallContext = createContext<CallContextValue | undefined>(undefined);

export const useCallContext = () => {
  /**
   * Consume CallContext with error handling
   * @public
   *
   * returns {Context}    CallContext
   */
  const context = useContext(CallContext);
  if (context === undefined) {
    throw Error( "CallContext is undefined! Are you consuming CallContext outside of the CallContextProvider?" );
  }
  return context;
};

interface Props {
  children: JSX.Element;
}

const CallContextProvider: React.FC<Props> = (props: Props) => {
  /**
   * Context for the calling-; signaling- & renegotiation-process
   * also provides MediaStream & Call Settings State
   * @component
   */
  const socketContext: SocketContextValue = useSocketContext();
  const userContext: UserContextValue = useUserContext();
  const notifContext: NotificationContextValue = useNotificationContext();
  const modalContext: ModalContextValue = useModalContext();

  /**
   * In React setTimoute will close all props/state
   * For example if you subscribe to an ID, and later want to unsubscribe, it would be a bug if ID could change over time
   * but in this case we want to have the current/latest/updated value of socketContext.state
   * to break out of the setTimeout loop in sendMessage/deleteGloballyMessage etc.
   * 
   * Source:
   * Cristian Salcescu
   * https://medium.com/programming-essentials/how-to-access-the-state-in-settimeout-inside-a-react-function-component-39a9f031c76f
   * 
   * https://github.com/facebook/react/issues/14010
   */
  const socketStateRef = useRef<SocketState>(socketContext.state);
  socketStateRef.current = socketContext.state;

  const [peerConnection, setPeerConnection] = useState<RTCPeerConnection | null>(null);
  const [icecandidates, setIcecandidates] = useState<Array<RTCIceCandidate | null>>([]);
  const [localStream, setLocalStream] = useState<MediaStream | null>(null);
  const [remoteStream, setRemoteStream] = useState<MediaStream | null>(null);
  const [remoteStreamVolume, setRemoteStreamVolume] = useState<number>(100); // 0 - 100
  const [screensharePeerConnection, setScreensharePeerConnection] = useState<RTCPeerConnection | null>(null);
  const [localScreenshareStream, setLocalScreenshareStream] = useState<MediaStream | null>(null);
  const [remoteScreenshareStream, setRemoteScreenshareStream] = useState<MediaStream | null>(null);
  const [screenshareIcecandidates, setScreenshareIcecandidates] = useState<Array<RTCIceCandidate | null>>([]);
  /* Web Audio API */
  const [remoteStreamAudio, setRemoteStreamAudio] = useState<HTMLAudioElement | null>(null);
  const [remoteStreamAudioContext, setRemoteStreamAudioContext] = useState<AudioContext | null>(null);
  const [remoteStreamGainNode, setRemoteStreamGainNode] = useState<GainNode | null>(null);
  const [remoteStreamDestination, setRemoteStreamDestination] = useState<MediaStreamAudioDestinationNode | null>(null);
  /* save socketids of remote to send socket events to the correct remote peer
   * could also be done with WebRTC but for now we'll use socket.io */
  const [localSocketid, setLocalSocketid] = useState<string | undefined>(undefined)
  const [remoteSocketid, setRemoteSocketid] = useState<string | undefined>(undefined);
  /* Key for security reasons => allow singaling only when the key matches the remote
   * Key gets thrown out at the point where the signaling process has been finished */
  const [key, setKey] = useState<undefined | string>(undefined); 
  const keyError = "HACKING ALERT! KEY IS NOT THE SAME";
  const [callingType, setCallingType] = useState<CallingType>(CallingType.Audio);
  const [calling, setCalling] = useState<number>(0); /* userid of who is calling */
  const [inCallWith, setInCallWith] = useState<number>(0); /* userid of whom you're in a call */
  const [callWindowLayout, setCallWindowLayout] = useState<Layout>(Layout.Mosaic);

  const [mic, setMic] = useState<boolean>(true); /* is Mic muted or not */
  const [showCam, setShowCam] = useState<boolean>(false);
  const [showScreen, setShowScreen] = useState<boolean>(false);
  const [remoteMic, setRemoteMic] = useState<boolean>(false);
  const [remoteShowCam, setRemoteShowCam] = useState<boolean>(true);
  const [remoteShowScreen, setRemoteShowScreen] = useState<boolean>(true);

  /**
   * TURN protocol runs top of STUN to setup a relay service.
   * A well written TURN server will also function as STUN;
   * so you can skip a "separate STUN server" option in such case. 
   */
  const peerConfig: RTCConfiguration = (process.env.NODE_ENV === "production") ?
				       {
					 iceServers: [
					   {
					     urls: [
					       "stun:turn.babbeln.app",
					     ],
					   },
					   {
					     username: "turnbabbelnapp",
					     credential: "turnbabbelnapppassword",
					     urls: [
					       "turn:turn.babbeln.app"
					     ],
					   }
					 ]
				       } : {};

  useEffect(() => {
    /**
     * Use Web Audio API to create <audio /> with GainNode
     * which will be then globally mounted to the DOM
     * 
     * FIXME: creates a whole new playback on OS level and doesn't free it though it's quite (doesn't send any data) 
     * 
     * @effect
     */
    if (remoteStream && remoteStreamAudioContext && remoteStreamGainNode && remoteStreamDestination && remoteStreamAudio) {
      /* add the remote stream to the audiocontext */
      const source: MediaStreamAudioSourceNode = remoteStreamAudioContext.createMediaStreamSource(remoteStream);
      /* connect gainNode to the new source */
      source.connect(remoteStreamGainNode);
      /* connect the audio destination to the new source */
      source.connect(remoteStreamDestination);
      /* debug: manipulate the gain for testing purposes */
      remoteStreamGainNode.gain.value = 2;
      remoteStreamAudioContext.resume();

      /* add the new remote stream as the source of the audio object */
      remoteStreamAudio.srcObject = source.mediaStream; 
      /* play that audio/stream */
      remoteStreamAudio.play();
    }

    return () => {
      /* Reset Audio Object */
      if (!remoteStreamAudio) return;
      remoteStreamAudio.pause();
      remoteStreamAudio.srcObject = null;
      remoteStreamAudio.load();
    }
  }, [remoteStream]);

  const resetState = () => {
    /**
     * This function should be called when creating the peer connection with the mediastream fails.
     * Reset all the state to the default value, that is required by the signaling process
     * @public
     */
    setPeerConnection(null);
    setIcecandidates([]);
    setLocalStream(null);
    setRemoteStream(null);
    setRemoteStreamVolume(100);
    setScreensharePeerConnection(null);
    setLocalScreenshareStream(null);
    setRemoteScreenshareStream(null);
    setScreenshareIcecandidates([]);
    // setRemoteStreamAudio(null);
    // setRemoteStreamAudioContext(null);
    // setRemoteStreamGainNode(null);
    // setRemoteStreamDestination(null);
    setLocalSocketid(undefined);
    setRemoteSocketid(undefined);
    setKey(undefined);
    setCallingType(CallingType.Audio);
    setCalling(0);
    setInCallWith(0);
    setMic(true);
    setShowCam(false);
    setShowScreen(false);
    setRemoteMic(false);
    setRemoteShowCam(false);
    setRemoteShowScreen(false);

    logger.log.magenta("CallContext", "Reset state.");
  };

  const reset = () => {
    /**
     * Reset state and clean up if the client is in a call
     * @public
     */
    resetState();
    // TODO handle logout/deleteaccount while in call
    // 1. stop the call
    // 2. reset the state
    // 3. finished
  };

  /* ----------------------------------------------------------------------------------------- ~Calling~ */
  const startCalling = (withUser: number, callingType: CallingType = CallingType.Audio) => {
    /**
     * Start a call with a remote friend.
     * this includes:
     *   1. emit socket event "start_call" to withUser with a newly generated key
     *   2. showing call modal & save key in state
     * @public
     * 
     * @params  {number}  withUser
     */
    if (!socketContext.socket) return;

    if (socketContext.state === SocketState.Disconnected) {
      logger.error.magenta("CallContext", "Disconnected from SocketServer while trying to start the calling process.")
      return notifContext.create(NotificationType.ERROR, `Internet connection problems. Please try again.`);
    }

    /* The key is already generated and safed in state,
     * because immediately after "accept_calling" the signaling process starts and I'm not sure
     * if react is fast enough to set the new state of the key intime for this process */
    const newKey: string = uuidv4();
    socketContext.maybeReconnect();
    logger.log.magenta("CallContext", "Socket event emitted start_calling");
    socketContext.socket.emit("start_calling", { fromUser: userContext.loggedInUserId, toUser: withUser, key: newKey });
    modalContext.setShowStartCallModal(true);
    setCalling(withUser);
    setCallingType(callingType);
    setKey(newKey);
  };

  const receiveCalling = (data: { fromUser: number; toUser: number; fromSocketid: string; key: string }) => {
    /**
     * Someone called this client.
     * This fn should only be used as a event listener for "start_calling" event.
     * @private
     * 
     * @params  {number}  data.fromUser      id of the user that emitted the "start_calling" event
     * @params  {number}  data.toUser        id to that the event was emitted
     * @params  {string}  data.fromSocketid  socketid of fromUser
     * @params  {string}  data.key           key for security purposes
     */

    /* FIXME Make this code bullet proof in the case many calls come in at the same time
     * BUT I don't know how I could wait for the first signalign process to finish before starting a new one
     * For now the state of calling tells us that this client is currently in the calling and/or signgaling
     * process, therefore the incomcing call will be sent into the void
     * the remote has to end the call and call again when no signaling process is currently going on */
    if (calling) return;

    setCalling(data.fromUser);
    setKey(data.key);
    setRemoteSocketid(data.fromSocketid);
    modalContext.setShowReceiveCallModal(true);
  };

  const endCalling = (data: { fromUser: number; toUser: number; toSocketid?: string }) => {
    /**
     * Stop calling somebody (this happens before Signaling => no peer connection yet established)
     * can be called when the client wants to cancel a call
     * or as socket event listener for "end_call" event
     * will emit socket event "end_calling" when fromUser is the same as the logged in user's id
     * This fn should only be used as a event listener for "end_calling" event.
     * @public
     * 
     * @params  {number}  data.fromUser      id of the user that emitted or will emit the "end_calling" event
     * @params  {number}  data.toUser        id to that the event was or should be emitted
     * @params  {string}  data.toSocketid    OPTIONAL if there is a socketid emit the event only to this socket
     *                                       if not emit it to all sockets of toUser
     *                                       this is because if the one starting the calling process ends the call
     *                                       then there is no remoteSocketid in his/her client's state & every toUser
     *                                       got the start_calling event we need to end_calling to all this sockets
     */
    const isClientInCall: boolean = (inCallWith && key && remoteSocketid) ? true : false;
    if (isClientInCall) return logger.error.yellow("CallContext", "end_calling socket event received but this client is currently in a call.");
    if (!socketContext.socket) return;

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => endCalling(data), 500);
      return logger.error.magenta("CallContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    if (data.fromUser === userContext.loggedInUserId) {
      /* only emit socket event "end_calling" if this client ended the incoming call */
      logger.log.magenta("CallContext", "Socket event emitted end_calling.");
      if (remoteSocketid) {
	/* if remoteSocketid => we received the "start_calling" event from the remote
	 * therefore we should only send the "end_calling" event only to the remoteSocketid */
	const endCallingData = { fromUser: data.fromUser, toUser: data.toUser, toSocketid: remoteSocketid };
	socketContext.socket.emit("end_calling", endCallingData);

	/* If this client declines a call then all the other clients that user is logged in
	 * should stop with the calling process. */
	logger.log.magenta("CallContext", "Socket event emitted client_end_calling");
	const clientEndCallingData = { userId: userContext.loggedInUserId, calling: calling };
	socketContext.socket.emit("client_end_calling", clientEndCallingData);
      } else {
	/* otherwise we started the calling process therefore the "end_calling" event should
	 * be sent to all devices that we're currently trying to call */
	const endCallingData = { fromUser: data.fromUser, toUser: data.toUser };
	socketContext.socket.emit("end_calling", endCallingData);
      }
    }
    notifContext.playSoundEndCall();
    /* Reset the UI */
    modalContext.setShowReceiveCallModal(false);
    modalContext.setShowStartCallModal(false);
    resetState();
  };

  const clientEndCalling= (data: { userId: number; calling: number }) => {
    /**
     * Listening function for event client_end_calling
     * Another client with the same user logged in accepted the call
     * therefore disable/reset the calling process on this client
     * without emitting "hangup"
     * @public
     * 
     * @params  {number}  userId     the user that initially emitted "clientend_calling"
     * @params  {number}  calling    the userid of the user that started the calling process (remote)
     */
    const isClientCurrentlyInCall: boolean = (inCallWith && key && remoteSocketid) ? true : false;
    const userIdMatches: boolean = data.userId === userContext.loggedInUserId;
    const callingMatches: boolean = data.calling === calling;

    if (!isClientCurrentlyInCall && userIdMatches && callingMatches) {
      logger.log.magenta("CallContext", "Calling process ended because of client_end_calling");
      /* Reset UI */
      modalContext.setShowReceiveCallModal(false);
      modalContext.setShowStartCallModal(false);
      resetState();
    } else {
      logger.error.magenta(
	"CallContext",
	`client_end_calling received but it didn't match: { userid: ${userContext.loggedInUserId}, calling: ${calling}), data: ${JSON.stringify(data)}`
      );
    }
  };

  const acceptCalling = (withUser: number) => {
    /**
     * Emit to the remoteSocketid that this client accepted the call,
     * so that both devices can start with the signalign process.
     * @public
     * 
     * @params  {number}  withUser
     */
    if (!socketContext.socket) return;

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => acceptCalling(withUser), 500);
      return logger.error.magenta("CallContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    logger.log.magenta("CallContext", "Socket event emitted accept_calling.");
    socketContext.socket.emit(
      "accept_calling",
      { fromUser: userContext.loggedInUserId, toUser: withUser, toSocketid: remoteSocketid! }
    );
    /* This client accepted the call, all the other clients receivecallmodals that this user is currently logged in
     * shouldn't be displayed anymore */
    logger.log.magenta("CallContext", "Socket event emitted client_end_calling.");
    socketContext.socket.emit(
      "client_end_calling",
      { userId: userContext.loggedInUserId, calling: withUser }
    );

    modalContext.setShowReceiveCallModal(false);
    modalContext.setShowStartCallModal(false);
  };

  /* ----------------------------------------------------------------------------------------- ~Signaling~ */
  const getPeerAndStream: (withUser: number, withSocketid: string) => Promise<[RTCPeerConnection | null, MediaStream | null]> = async (
    withUser,
    withSocketid
  ) => {
    /**
     * Initiates the peerConnection object:
     *   1. create RTCPeerConnection
     *   2. add events to RTCPeerConnection
     *   3. create MediaStream
     *   4. add events to MediaStream
     *   5. add MediaSteaem to RTCPeerConnection
     * @private
     * 
     * @params   {number}  withUser
     * @params   {string}  withSocketid
     * 
     * @returns  {RTCPeerConnection}  localPeer but as a Promise
     */

    /* Instantiate AudioContext etc. only if it hasn't been done before */
    const hasAudioContextBeenCreated: boolean = (remoteStreamAudioContext &&
						 remoteStreamGainNode &&
						 remoteStreamDestination &&
						 remoteStreamAudio) ? true : false;
    if (!hasAudioContextBeenCreated) {
      const context: AudioContext = new AudioContext();
      const gainNode: GainNode = context.createGain();
      const destination: MediaStreamAudioDestinationNode = context.createMediaStreamDestination();
      const audio: HTMLAudioElement = new Audio();

      setRemoteStreamAudioContext(context);
      setRemoteStreamGainNode(gainNode);
      setRemoteStreamDestination(destination);
      setRemoteStreamAudio(audio);
    }

    /* create mediastreams */
    const stream: MediaStream | null = await navigator.mediaDevices.getUserMedia({
      video: (callingType === CallingType.Video) ? true :false,
      audio: true,
    }).catch((err: unknown) => {
      notifContext.create(NotificationType.ERROR, "Couldn't get your media devices. Maybe your browser disabled this feature?");
      console.error(`Couldn't get your media devices: ${err}`);
      return null;
    });

    /* if getUserMedia fails stop!!!! */
    if (!stream) return [null, stream]; // same as return [null, null]

    /* mute audio tracks if mic is disabled */
    if (!mic) stream!.getAudioTracks().map((track: MediaStreamTrack) => track.enabled = false);

    /* initiate peerConnection */
    const newPeerConnection: RTCPeerConnection = new RTCPeerConnection(peerConfig);

    /* add icecandidate event listener
     * if a new icecandidate is found emit it to the remote peer */
    newPeerConnection.addEventListener("icecandidate", (event: RTCPeerConnectionIceEvent) => {
      const icecandidate: RTCIceCandidate | null = event.candidate;
      if (socketContext.socket) {
	logger.log.magenta("CallContext", "Socket event emitted signaling_icecandidate");
	socketContext.socket.emit(
	  "signaling_icecandidate",
	  {
	    fromUser: userContext.loggedInUserId, /* the id of the user emitting the event */
	    toUser: withUser, /* the id of the user to whom the candidate should be sent */
	    toSocketid: withSocketid, /* the socketid of the device of the remote user */
	    candidate: icecandidate, /* candidate */
	    key: key!, /* the security key to make the signaling process more secure */
	  }
	);
      }
    });

    newPeerConnection.addEventListener("negotiationneeded", (event: Event) => {
      logger.log.magenta("CallContext", "peerConnection needs (re)negotiation.");
    })

    /* update remoteStream every time the remote peer changes its tracks */
    newPeerConnection.addEventListener("track", (event: RTCTrackEvent) => {
      const newRemoteStream: MediaStream = event.streams[0];
      setRemoteStream(newRemoteStream);
    });

    /* if remote peer loses connection close the call */
    newPeerConnection.addEventListener("connectionstatechange", (_event: Event) => {
      logger.log.magenta("CallContext", `Connection State changed: ${newPeerConnection.connectionState}`);
      if (newPeerConnection.connectionState === "disconnected") return hangup("on");
      if (newPeerConnection.connectionState === "connected") return notifContext.playSoundStartCall();
      if (newPeerConnection.connectionState === "failed") return startRenegotiation();
    });

    /* add the local stream to the local peerConnection */
    stream.getTracks().forEach((track: MediaStreamTrack) => {
      newPeerConnection!.addTrack(
	track,
	stream, /* stream for synchronizing the tracks */
      );
    });

    return [newPeerConnection, stream];
  };

  const signalingStartCall = async (data: { fromUser: number; toUser: number; fromSocketid: string; toSocketid: string }) => {
    /**
     * calling process has been finished & this client starts the signaling process by:
     *   1. creating peerConnection & mounting the neccessary event listeners
     *   2. creating an offer
     *   3. emitting that offer to the remote
     * This fn should only be used as a event listener for "accept_calling" event
     * @public
     * 
     * @params  {number}  data.fromUser      id of the user that emitted the "accept_calling" event
     * @params  {number}  data.toUser        id to that the event was emitted
     * @params  {string}  data.fromSocketid  socketid of fromUser
     * @params  {string}  data.toSocketid    socketid of toUser
     */

    // TODO FIXME It could happen that after 83sec the friend accepts the call, but because of inet delay
    // the socket event + ui update takes more than 1-2secs and signalingStartCall gets called even though
    // the callcontext has been reseted
    // if thats the case don't allow singalingStartCall to be carried on!!!
    // smthng like this:
    // if isNotInCallingProcess && isNotInCall return;
    const isInCallingProcessOrInCall: boolean = (calling || inCallWith) ? true : false;
    if (!isInCallingProcessOrInCall) return logger.error.yellow("CallContext", "Client is not in calling process nor in a call. Aborted signalingStartCall");
    if (!key) return notifContext.create(NotificationType.ERROR, "KEY IS UNDEFINED BUT IT SHOULDN'T.");

    /* unmount all call modals */
    modalContext.setShowReceiveCallModal(false);
    modalContext.setShowStartCallModal(false);

    /* init pc */
    const [newPeerConnection, stream]: [RTCPeerConnection | null, MediaStream | null] = await getPeerAndStream(data.fromUser, data.fromSocketid);

    if (!newPeerConnection || !stream) {
      logger.error.magenta("CallContext", "PeerConnection and/or local Stream is null.");
      return hangup("emit");
    }
    /* create & emit offer to remote */
    const offer: RTCSessionDescriptionInit = await newPeerConnection.createOffer();
    /* set offer as local description */
    /* which will also trigger "icecandidate" event in the future */
    await newPeerConnection.setLocalDescription(offer); 

    /* emit the offer to remote */
    if (socketContext.socket) {
      logger.log.magenta("CallContext", "Socket event emitted signaling_offer.");
      socketContext.socket.emit(
	"signaling_offer",
	{
	  fromUser: userContext.loggedInUserId,
	  toUser: data.fromUser,
	  toSocketid: data.fromSocketid,
	  /* the backend server will add fromSocketid & send it to the remote */
	  offer,
	  key,
	}
      );
    }

    setRemoteSocketid(data.fromSocketid);
    setLocalStream(stream);
    setPeerConnection(newPeerConnection);
  };

  const signalingReceiveCall = async (data: {
    fromUser: number;
    toUser: number;
    fromSocketid: string;
    toSocketid: string;
    offer: RTCSessionDescriptionInit;
    key: string
  }) => {
    /**
     * The remote peer started with the signaling process, so this fn will:
     *   1. init pc
     *   2. set offer as remote description
     *   3. create answer
     *   4. emit answer to remote peer
     * This fn should only be used as a event listener for "signaling_offer" event
     * @private
     * 
     * @params  {number}  data.fromUser      id of the user that emitted the "signaling_offer" event
     * @params  {number}  data.toUser        id to that the event was emitted
     * @params  {string}  data.fromSocketid  socketid of fromUser
     * @params  {string}  data.toSocketid    socketid of toUser
     * @params  {RTCSessionDescriptionInit}  data.offer
     * @params  {string}  data.key           key for security purpose
     */
    if (data.key !== key) return notifContext.create(NotificationType.ERROR, keyError);

    const [newPeerConnection, stream]: [RTCPeerConnection | null, MediaStream | null] = await getPeerAndStream(data.fromUser, remoteSocketid!);

    if (!newPeerConnection || !stream) {
      logger.error.magenta("CallContext", "PeerConnection and/or local Stream is null.");
      return hangup("emit");
    }
    
    /* set offer as remote description */
    await newPeerConnection.setRemoteDescription(data.offer);
    /* create answer */
    const answer: RTCSessionDescriptionInit = await newPeerConnection.createAnswer();
    await newPeerConnection.setLocalDescription(answer);
    /* emit the answer to remote */
    if (socketContext.socket) {
      logger.log.magenta("CallContext", "Socket event emitted signaling_answer.");
      socketContext.socket.emit(
	"signaling_answer",
	{
	  fromUser: data.toUser,
	  toUser: data.fromUser,
	  fromSocketid: data.toSocketid,
	  toSocketid: data.fromSocketid,
	  answer: answer,
	  key: key
	}
      );
    }

    setInCallWith(data.fromUser);

    /* set peerConnection as state to make it globally available */
    setLocalStream(stream);
    setPeerConnection(newPeerConnection);
  };

  const signalingFinalizeConnection = async (
    data: {
      fromUser: number;
      toUser: number;
      fromSocketid: string;
      toSocketid: string;
      answer: RTCSessionDescriptionInit;
      key: string
    }
  ) => {
    /**
     * remote peer did send an answer now the signaling process can be completed
     * a p2p connection can be established
     * This fn should only be used as a event listener for "signaling_answer" event
     * @private
     * 
     * @params  {number}  data.fromUser      id of the user that emitted the "signaling_answer" event
     * @params  {number}  data.toUser        id to that the event was emitted
     * @params  {string}  data.fromSocketid  socketid of fromUser (added by the back-end server)
     * @params  {string}  data.toSocketid    socketid of toUser
     * @params  {RTCSessionDescriptionInit}  data.answer
     * @params  {string}  data.key           key for security purpose
     */
    if (data.key !== key) return notifContext.create(NotificationType.ERROR, keyError);
    if (!peerConnection) return notifContext.create(NotificationType.ERROR, "Peer Connection is null!!!");

    await peerConnection.setRemoteDescription(data.answer);
    const withUser: number = (userContext.loggedInUserId === data.fromUser) ? data.toUser : data.fromUser;

    setInCallWith(withUser);
  };

  const gotIceCandidate = (data: { fromUser: number; toUser: number; candidate: RTCIceCandidate | null; key: string }) => {
    /**
     * When the client receives a "signaling_icecandidate" add this icecandidate to the icecadidates state
     * & add it to local peer connection after remote description has been set!
     * @private
     * 
     * @params  {[object Object]} data passed down from socket event "signaling_icecanidate"
     */
    if (!key) return; // FIXME skip if key is undefined bcause I can't figure out how to handle if the getPeerAndStream fn failes...
    // if (data.key !== key) return notifContext.create(NotificationType.ERROR, keyError);
    if (data.key !== key) return;
    return setIcecandidates([...icecandidates, data.candidate]);
  };

  const hangup = (event: "emit" | "on" = "emit") => {
    /**
     * Close the Peer Connection and reset the callcontext state 
     * if the param event is "emit" also emit a socket "hangup" event to the remote peer
     * @public
     * 
     * @params  {"emit" | "on"}  event: default "emit"
     */

    /* emit hangup event to remote */
    if (socketContext.socket && event === "emit") {
      logger.log.magenta("CallContext", "Socket event emitted hangup.");
      socketContext.socket.emit("hangup", { fromUser: userContext.loggedInUserId, toUser: inCallWith, toSocketid: remoteSocketid! });
    }
    if (peerConnection) { peerConnection.close(); }
    if (localStream) { localStream.getTracks().map((track: MediaStreamTrack) => track.stop()); }

    if (remoteStreamAudioContext) {
      remoteStreamAudioContext.suspend();  
    }

    notifContext.playSoundEndCall();
    resetState();
  };

  /* ----------------------------------------------------------------------------------------- ~Screenshare~ */
  const getScreensharePeer: () => RTCPeerConnection = () => {
    /**
     * Create Peer Connection for screenshare
     * @private
     * 
     * @returns  { RTCPeerConnection }  screensharePeerConnection
     */

     /* initiate peerConnection */
    const newScreensharePeerConnection: RTCPeerConnection = new RTCPeerConnection(peerConfig);

    /* add icecandidate event listener
     * if a new icecandidate is found emit it to the remote peer */
    newScreensharePeerConnection.addEventListener("icecandidate", (event: RTCPeerConnectionIceEvent) => {
      const icecandidate: RTCIceCandidate | null = event.candidate;
      if (socketContext.socket && remoteSocketid) {
	logger.log.magenta("CallContext", "Socket event emitted screenshare_icecandidate");
	socketContext.socket.emit(
	  "screenshare_icecandidate",
	  {
	    toSocketid: remoteSocketid,
	    candidate: icecandidate,
	    key: key!,
	  },
	);
      }
    });

    newScreensharePeerConnection.addEventListener("negotiationneeded", (event: Event) => {
      logger.log.magenta("CallContext", "screensharePeerConnection needs (re)negotiation.");
    });

    /* update remoteScreenshareStream every time the remote peer changes its tracks */
    newScreensharePeerConnection.addEventListener("track", (event: RTCTrackEvent) => {
      notifContext.playSoundStartScreenshare(); // play sound when remote creates a new stream
      const newRemoteScreenshareStream: MediaStream = event.streams[0];
      setRemoteScreenshareStream(newRemoteScreenshareStream);
    });

    /* if remote peer loses connection close the call */
    newScreensharePeerConnection.addEventListener("connectionstatechange", (_event: Event) => {
      logger.log.magenta("CallContext", `Connection State (of screensharePeerConnection) changed: ${newScreensharePeerConnection.connectionState}`);
      if (newScreensharePeerConnection.connectionState === "disconnected") return; // reset screenshare state
      // if (newScreensharePeerConnection.connectionState === "connected") return notifContext.playSoundStartScreenshare();
      if (newScreensharePeerConnection.connectionState === "failed") return newScreensharePeerConnection.restartIce();
    });

    return newScreensharePeerConnection;
  };

  const startScreenshareSignaling = async (stream: MediaStream) => {
    /**
     * Start screen share stream & peer connection
     * @public
     * 
     * @param  {MediaStram}  the screenshare stream that gets added
     *                       to peer connection and stored in state
     */
    if (!socketContext.socket) return logger.error.magenta("CallContext", "Socket is null.");
    if (!remoteSocketid) return logger.error.magenta("CallContext", "remoteSocketid is null.");
    if (!key) return logger.error.magenta("CallContext", "Key is null.");

    /* if the screensharePeerConnection already exists this means that the remote is already streaming
     * so after adding the stream to pc we'll need to renegotiate with the remote */
    const needToRenegotiate: boolean = (screensharePeerConnection) ? true : false;
    /* screensharePeerConnection will be either the current state or a newly created peer connection */
    const scopedScreensharePeerConnection: RTCPeerConnection = (screensharePeerConnection) ? screensharePeerConnection : getScreensharePeer();

    /* add the local stream to the local peerConnection */
    stream.getTracks().map((track: MediaStreamTrack) => {
      scopedScreensharePeerConnection.addTrack(
	track,
	stream /* stream for synchronizing the tracks */
      );
    });

    const offer: RTCSessionDescriptionInit  = await scopedScreensharePeerConnection.createOffer();
    await scopedScreensharePeerConnection.setLocalDescription(offer);

    if (needToRenegotiate) {
      logger.log.magenta("CallContext", "Socket event emitted renegotiate_screenshare_offer");
      socketContext.socket.emit(
	"screenshare_renegotiation_offer",
	{
	  toSocketid: remoteSocketid,
	  offer: offer,
	  key: key
	},
      );
    } else {
      logger.log.magenta("CallContext", "Socket event emitted screenshare_offer");
      socketContext.socket.emit(
	"screenshare_offer",
	{
	  toSocketid: remoteSocketid,
	  offer: offer,
	  key: key
	},
      );
    }

    setLocalScreenshareStream(stream);
    setScreensharePeerConnection(scopedScreensharePeerConnection);
  };

  const receivedScreenshareSignaling = async (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }) => {
    /**
     * Listener function for the "screenshare_offer" socket event
     * answer with "screenshare_answer" event
     * @private
     * 
     * @param  {string}                      data.toSocketid  this clients socketid
     * @param  {RTCSessionDescriptionInit}   data.offer       webrtc offer
     * @param  {key}                         data.key         session key for this call
     */
    if (data.key !== key) {
      logger.error.magenta("CallContext", "Key Error in received screeshare signaling.");
      return notifContext.create(NotificationType.ERROR, keyError);
    }

    const newScreensharePeerConnection: RTCPeerConnection = getScreensharePeer();

    await newScreensharePeerConnection.setRemoteDescription(data.offer);
    const answer: RTCSessionDescriptionInit = await newScreensharePeerConnection.createAnswer();
    await newScreensharePeerConnection.setLocalDescription(answer);

    if (socketContext.socket && remoteSocketid) {
      logger.log.magenta("CallContext", "Socket event emitted screenshare_answer");
      socketContext.socket.emit(
	"screenshare_answer",
	{
	  toSocketid: remoteSocketid,
	  answer: answer,
	  key: key!,
	},
      );
    }

    setScreensharePeerConnection(newScreensharePeerConnection);
  };

  const finalizeScreenshareSignaling = async (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }) => {
    /**
     * Listener function for "screenshare_answer"
     * An offer has been sent and the remote answered with an answer
     * @private
     * 
     * @param  {string}                     data.toSocketid  this client's socketid
     * @param  {RTCSessionDescriptionInit}  data.answer      webrtc answer of the remote
     * @param  {string}                     data.key         session key for this call
     */
    if (data.key !== key) {
      logger.error.magenta("CallContext", "Key Error finalizing screeshare signaling process.");
      return notifContext.create(NotificationType.ERROR, keyError);
    }
    if (!screensharePeerConnection) {
      logger.error.magenta("CallContext", "screensharePeerConnectio is null in finalizing screenshare signaling process.");
      return notifContext.create(NotificationType.ERROR, "Screenshare Peer Connection is null!!!");
    }

    await screensharePeerConnection.setRemoteDescription(data.answer);
  };

  const startScreenshareRenegotiation = async () => {
    /**
     * start renegotiation process for screenshare,
     * because this client added/removed a screenshare stream
     * @public
     */
    if (!socketContext.socket) return logger.error.magenta("CallContext", "Socket is null.");
    if (!screensharePeerConnection) return logger.error.magenta("CallContext", "screenhsarePeerConnection is null.");
    if (!remoteSocketid) return logger.error.magenta("CallContext", "remoteSocketid is null.");
    if (!key) return logger.error.magenta("CallContext", "key is null.");

    const offer: RTCSessionDescriptionInit  = await screensharePeerConnection.createOffer();
    await screensharePeerConnection.setLocalDescription(offer);
     
    logger.log.magenta("CallContext", "Socket event emitted screenshare_renegotiation_offer.");
    socketContext.socket.emit(
      "screenshare_renegotiation_offer",
      {
	toSocketid: remoteSocketid,
	offer: offer,
	key: key,
      },
    );
  };

  const receivedScreenshareRenegotiation = async (data: { toSocketid: string; offer: RTCSessionDescriptionInit; key: string }) => {
    /**
     * Listener function for the socket event "screenshare_renegotiation_offer".
     * Create answer and emit it to the remote
     * @private
     * 
     * @param  {string}                     data.toSocketid  this client's socketid
     * @param  {RTCSessionDescriptionInit}  data.offer       webrtc offer of the remote
     * @param  {string}                     data.key         session key for this call
     */
    if (data.key !== key) {
      logger.error.magenta("CallContext", "Key Error finalizing screeshare renegotiation process.");
      return notifContext.create(NotificationType.ERROR, keyError);
    }
    // FIXME gets called when the screenshare peer connection gets removed!
    if (!screensharePeerConnection) {
      return logger.error.magenta("CallContext", "screensharePeerConnectio is null in screenshare renegotiation process.");
    }
    if (!socketContext.socket) return logger.error.magenta("CallContext", "Socket is null.");
    if (!remoteSocketid) return logger.error.magenta("CallContext", "remoteSocketid is null.");

    await screensharePeerConnection.setRemoteDescription(data.offer);
    const answer: RTCSessionDescriptionInit = await screensharePeerConnection.createAnswer();
    await screensharePeerConnection.setLocalDescription(answer);

    logger.log.magenta("CallContext", "Socket event emitted renegotiation_screenshare_answer.");
    socketContext.socket.emit(
      "screenshare_renegotiation_answer",
      {
	toSocketid: remoteSocketid,
	answer: answer,
	key: key },
    );
  };

  const finalizeScreenshareRenegotiation = async (data: { toSocketid: string; answer: RTCSessionDescriptionInit; key: string }) => {
    /**
     * Listener function for the socket event "screenshare_renegotiation_answer".
     * @private
     * 
     * @param  {string}                     data.toSocketid  this client's socketid
     * @param  {RTCSessionDescriptionInit}  data.answer      webrtc answer of the remote
     * @param  {string}                     data.key         session key for this call
     */
     if (data.key !== key) {
      logger.error.magenta("CallContext", "Key Error finalizing screeshare renegotiation process.");
      return notifContext.create(NotificationType.ERROR, keyError);
    }
    if (!screensharePeerConnection) {
      logger.error.magenta("CallContext", "screensharePeerConnectio is null in finalizing screenshare renegotiation process.");
      return notifContext.create(NotificationType.ERROR, "Screenshare Peer Connection is null!!!");
    }
  
    await screensharePeerConnection.setRemoteDescription(data.answer);
  };

  const screenshareClose = () => {
    /**
     * Emit to the remote "screenshare_close" event
     * to let the remote know that we removed the screenshare stream
     * @public
     */
    if (!socketContext.socket) return logger.error.magenta("CallContext", "Socket is null.");
    if (!screensharePeerConnection) return logger.error.magenta("CallContext", "screensharePeerConnection is null.");
    if (!remoteSocketid) return logger.error.magenta("CallContext", "remoteSocketid is null.");
    if (!key) return logger.error.magenta("CallContext", "Key is null.");

    logger.log.magenta("CallContext", "Socket event emitted screenshare_close.");
    socketContext.socket.emit(
	"screenshare_close",
	{
	  toSocketid: remoteSocketid,
	  key: key,
	},
      );

    /* remove old stream from peer */
    const senders: Array<RTCRtpSender> = screensharePeerConnection.getSenders();
    senders.map((sender: RTCRtpSender) => screensharePeerConnection.removeTrack(sender));

    /* renegotiate if remote is streaming */
    if (remoteScreenshareStream) { startScreenshareRenegotiation(); }

    setLocalScreenshareStream(null);
  };

  const receivedScreenshareClose = (data: { toSocketid: string; key: string; }) => {
    /**
     * Listener function for "screenshare_close" socket event
     * The remote stopped/removed its screenshare media stream this client should
     * do the same
     * @private
     * 
     * @param  {string}  toSocketid  socketid of this client
     * @param  {string}  key         session key for this client
     */
    if (data.key !== key) return logger.error.magenta("CallContext", "Key doesn't match.");
    setRemoteScreenshareStream(null);
  };

  const gotScreenshareIcecandidate = (data: { toSocketid: string; candidate: RTCIceCandidate | null; key: string }) => {
    /**
     * Listener function for "screenshare_icecandidate" socket event
     * @private
     * 
     * @param  {string}                  toSocketid  socketid of this client
     * @param  {RTCIceCandidate | null}  candidate   icecandidate or null (null indicates the end)
     * @param  {string}  key             session key for this client
     */
    if (!key) return;
    if (data.key !== key) return;
    return setScreenshareIcecandidates([...screenshareIcecandidates, data.candidate]);
  };

  useEffect(() => {
    /**
     * Remove/Drop screenshare peer connection
     * if there is no active stream
     * @effect
     */

    // TODO instead of removing the peer connection
    // play sound every time a new stream is added instead
    // of when the p2p connection is created (like it is in the normal pc)

    if (!localScreenshareStream && !remoteScreenshareStream) {
      setScreensharePeerConnection(null);
    }
  }, [localScreenshareStream, remoteScreenshareStream]);

  /* ----------------------------------------------------------------------------------------- ~Renegotiation~ */
  const startRenegotiation = async () => {
    /**
     * Start renegotiation process:
     * Create offer & send to remote as "renegotiation_offer" event
     * @public
     */
    if (!key) return notifContext.create(NotificationType.ERROR, keyError);
    if (!peerConnection) return notifContext.create(NotificationType.ERROR, "Renegotiation: peerConnection is null!");
    if (!socketContext.socket) return notifContext.create(NotificationType.ERROR, "Renegotiation: Socket is null!");
    if (!remoteSocketid) return notifContext.create(NotificationType.ERROR, "Renegotiation: remoteSocketid is undefined!");

    const offer: RTCSessionDescriptionInit = await peerConnection.createOffer();
    await peerConnection.setLocalDescription(offer);

    logger.log.magenta("CallContext", "Socket event emitted renegotiation_offer.");
    return socketContext.socket.emit(
      "renegotiation_offer",
      { fromUser: userContext.loggedInUserId, toUser: inCallWith, toSocketid: remoteSocketid, offer: offer, key: key }
    );
  };

  const receiveRenegotiation = async (
    data: { fromUser: number; toUser: number; toSocketid: string; offer: RTCSessionDescriptionInit; key: string }
  ) => {
    /**
     * Got an offer from remote set this as remote description & create an answer
     * emit this answer with the "renegotiation_answer" socket event
     * This fn should only be used as a event listener for "renegotiation_offer" event
     * @private
     * 
     * @params  {number}  data.fromUser      id of the user that emitted the "renegotiation_offer" event
     * @params  {number}  data.toUser        id to that the event was emitted
     * @params  {string}  data.toSocketid    socketid of the toUser
     * @params  {RTCSessionDescriptionInit}  data.offer
     * @params  {string}  data.key           key for security purpose
     */
    if (data.key !== key) return notifContext.create(NotificationType.ERROR, keyError);
    if (!peerConnection) return notifContext.create(NotificationType.ERROR, "Renegotiation: peerConnection is null!");
    if (!socketContext.socket) return;
    if (!remoteSocketid) return;

    await peerConnection.setRemoteDescription(data.offer);
    const answer: RTCSessionDescriptionInit = await peerConnection.createAnswer();
    await peerConnection.setLocalDescription(answer);

    logger.log.yellow("CallContext", "Socket event emitted renegotiation_answer.");
    return socketContext.socket.emit(
      "renegotiation_answer",
      { fromUser: userContext.loggedInUserId, toUser: inCallWith, toSocketid: remoteSocketid, answer: answer, key: key }
    );
  };

  const finalizeRenegotiation = async (
    data: { fromUser: number; toUser: number; toSocketid: string; answer: RTCSessionDescriptionInit; key: string }
  ) => {
    /**
     * Got an answer from remote set this as remote description
     * re-negotioation-process is finished a new p2p connection with/-out video stream
     * This fn should only be used as a event listener for "renegotiation_answer" event
     * @private
     * 
     * @params  {number}  data.fromUser      id of the user that emitted the "renegotiation_answer" event
     * @params  {number}  data.toUser        id to that the event was emitted
     * @params  {string}  data.toSocketid    socketid of the toUser
     * @params  {RTCSessionDescriptionInit}  data.answer
     * @params  {string}  data.key           key for security purpose
     */
    if (data.key !== key) return notifContext.create(NotificationType.ERROR, keyError);
    if (!peerConnection) return notifContext.create(NotificationType.ERROR, "Renegotiation: peerConnection is null!");

    await peerConnection.setRemoteDescription(data.answer);
  };

  /* ----------------------------------------------------------------------------------------- ~Reconnected~ */
  const callingReconnected = (socketid: string) => {
    /**
     * If the client loses it's connection to the socket server, on reconnecting it will get
     * a new socket id assigned.
     * When the client is in a call, `callingReconnected` will try to send/get the updated socketid
     * from the client he/she is currently in call with
     * @public
     */

    /* Abort if client isn't currently in a call */
    if (!key || !inCallWith) return logger.log.yellow("CallContext", `callingReconnected: ${JSON.stringify({key: key, inCallWith: inCallWith})}`);

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => callingReconnected(socketid), 500);
      return logger.error.magenta("CallContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    setLocalSocketid(socketid);

    if (!socketContext.socket) return;
    if (!userContext.loggedInUserId) return;

    logger.log.magenta("CallContext", "Socket event emitted calling_reconnected.");
    return socketContext.socket.emit(
      "calling_reconnected",
      {
	key: key,
	fromUser: userContext.loggedInUserId,
	fromSocketid: socketid,
	inCallWith: inCallWith,
      }
    );
  };

  const handleCallingReconnected = (data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number }) => {
    /**
     * The client we might be currently in a call with might have lost its connection to the socket server
     * He/she triggered the event "calling_reconnect". If our friend is currently in call with this client
     * respond with an answer "calling_reconnect_answer"
     * @private
     * 
     * @params  {string}  data.key           session key for this call
     * @params  {number}  data.fromUser      the user id that emitted the "calling_reconnected" event
     * @params  {string}  data.fromSocketid  the socket id of that client
     * @params  {number}  data.toUser        with which user he was in a call with (this should be this client's user id)
     * 
     * @returns  {void}   socket.emit("calling_reconnected_answer")
     */
    logger.log.yellow("CallContext", `handleCallingReconnected: ${JSON.stringify({ data })}`);

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => handleCallingReconnected(data), 500);
      return logger.error.magenta("CallContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    if (!socketContext.socket) return;
    if (!localSocketid) return;

    const keyMatches: boolean = data.key === key;
    const inCallWithMatches: boolean = data.inCallWith === userContext.loggedInUserId;

    const answerData = {
      key: data.key,
      fromUser: data.fromUser, // in this case fromUser refers to the remote user
      fromSocketid: data.fromSocketid,
      inCallWith: data.inCallWith, // refers to this client
      inCallWithSocketid: localSocketid,
    };

    if (keyMatches && inCallWithMatches) {
      setRemoteSocketid(data.fromSocketid);
      logger.log.magenta("CallContext", "Socket event emitted calling_reconnected_answer.");
      return socketContext.socket.emit("calling_reconnected_answer", answerData);
    }
  };

  const handleCallingReconnectedAnswer = (data: { key: string; fromUser: number; fromSocketid: string; inCallWith: number; inCallWithSocketid: string }) => {
    /**
     * This client lost its connection to the socket server. It emitted "calling_reconnected" and now
     * the client that is currently in a call with this client answered.
     * Update the remotes socket id
     * @private
     * 
     * @params  {string}  data.key                 session key for this call
     * @params  {number}  data.fromUser            the user id that emitted the "calling_reconnected" event
     * @params  {string}  data.fromSocketid        the socket id of that client
     * @params  {number}  data.toUser              with which user he was in a call with (this should be this client's user id)
     * @params  {number}  data.toUserSocketid      the socket id of the other client
     */
    logger.log.yellow("CallContext", `handleCallingReconnectedAnswer: ${JSON.stringify({ data })}`);

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => handleCallingReconnectedAnswer(data), 500);
      return logger.error.magenta("CallingContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    if (!socketContext.socket) return;

    const keyMatches: boolean = data.key === key;
    const inCallWithMatches: boolean = data.inCallWith === inCallWith;

    if (keyMatches && inCallWithMatches) return setRemoteSocketid(data.inCallWithSocketid);
  };

  useEffect(() => {
    /**
     * Add ice candidates only after remote description is set
     * @effect
     * 
     * @dependencies  {RTCIceCandidate[]}  icecandidates that haven't been added to peerConnection yet
     * @dependencies  {RTCPeerConnection}  local peer connection
     */
    if (peerConnection?.remoteDescription && icecandidates.length) {
      icecandidates.map(async (candidate: RTCIceCandidate | null) => {
	if (candidate) return await peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
	/* If candidate is null then all icecandidates have been sent:
	 * Calling/Signaling process has been finished */
	if (!candidate && calling) return setCalling(0);
      });
      setIcecandidates([]);
    }
  }, [icecandidates, peerConnection]);

  useEffect(() => {
    /**
     * Add ice candidates only after remote description of screensharePeerConnection is set
     * @effect
     * 
     * @dependencies  {RTCIceCandidate[]}  icecandidates that haven't been added to peerConnection yet
     * @dependencies  {RTCPeerConnection}  local screenshare peer connection
     */
    if (screensharePeerConnection?.remoteDescription && screenshareIcecandidates.length) {
      screenshareIcecandidates.map(async (candidate: RTCIceCandidate | null) => {
	if (candidate) return await screensharePeerConnection.addIceCandidate(new RTCIceCandidate(candidate));
      });
      setScreenshareIcecandidates([]);
    }
  }, [screenshareIcecandidates, screensharePeerConnection]);

  useEffect(() => { 
    if (socketContext.socket) {
      socketContext.socket.on(
	"start_calling",
	(data: {
	  fromUser: number;
	  toUser: number;
	  fromSocketid: string;
	  key: string
	}) => receiveCalling(data)
      );
      socketContext.socket.on(
	"end_calling",
	(data: {
	  fromUser: number;
	  toUser: number;
	  toSocketid?: string
	}) => endCalling(data)
      );
      socketContext.socket.on(
	"client_end_calling",
	(data: {
	  userId: number;
	  calling: number;
	}) => clientEndCalling(data)
      );
      socketContext.socket.on(
	"accept_calling",
	(data: {
	  fromUser: number;
	  toUser: number;
	  fromSocketid: string;
	  toSocketid: string
	}) => signalingStartCall(data)
      );
      socketContext.socket.on(
	"signaling_icecandidate",
	(data: {
	  fromUser: number;
	  toUser: number;
	  toSocketid: string;
	  candidate: RTCIceCandidate | null;
	  key: string
	}) => gotIceCandidate(data)
      );
      socketContext.socket.on(
	"signaling_offer",
	(data: {
	  fromUser: number;
	  toUser: number;
	  fromSocketid: string;
	  toSocketid: string;
	  offer: RTCSessionDescriptionInit;
	  key: string
	}) => signalingReceiveCall(data)
      );
      socketContext.socket.on(
	"signaling_answer",
	(data: {
	  fromUser: number;
	  toUser: number;
	  fromSocketid: string;
	  toSocketid: string;
	  answer: RTCSessionDescriptionInit;
	  key: string
	}) => signalingFinalizeConnection(data)
      );
      socketContext.socket.on(
	"screenshare_offer",
	(data: {
	  toSocketid: string;
	  offer: RTCSessionDescriptionInit;
	  key: string;
	}) => receivedScreenshareSignaling(data)
      );
      socketContext.socket.on(
	"screenshare_answer",
	(data: {
	  toSocketid: string;
	  answer: RTCSessionDescriptionInit;
	  key: string;
	}) => finalizeScreenshareSignaling(data)
      );
      socketContext.socket.on(
	"screenshare_icecandidate",
	(data: {
	  toSocketid: string;
	  candidate: RTCIceCandidate | null,
	  key: string;
	}) => gotScreenshareIcecandidate(data)
      );
      socketContext.socket.on(
	"screenshare_renegotiation_offer",
	(data: {
	  toSocketid: string;
	  offer: RTCSessionDescriptionInit;
	  key: string;
	}) => receivedScreenshareRenegotiation(data)
      );
      socketContext.socket.on(
	"screenshare_renegotiation_answer",
	(data: {
	  toSocketid: string;
	  answer: RTCSessionDescriptionInit;
	  key: string;
	}) => finalizeScreenshareRenegotiation(data)
      );
      socketContext.socket.on(
	"screenshare_close",
	(data: {
	  toSocketid: string;
	  key: string;
	}) => receivedScreenshareClose(data)
      );
      socketContext.socket.on(
	"hangup",
	(data: {
	  fromUser: number;
	  toUser: number;
	  toSocketid: string
	}) => hangup("on")
      );
      socketContext.socket.on(
	"renegotiation_offer",
	(data: {
	  fromUser: number;
	  toUser: number;
	  toSocketid: string;
	  offer: RTCSessionDescriptionInit;
	  key: string;
	}) => receiveRenegotiation(data)
      );
      socketContext.socket.on(
	"renegotiation_answer",
	(data: {
	  fromUser: number;
	  toUser: number;
	  toSocketid: string;
	  answer: RTCSessionDescriptionInit;
	  key: string;
	}) => finalizeRenegotiation(data)
      );
      socketContext.socket.on(
	"calling_reconnected",
	(data: {
	  key: string;
	  fromUser: number;
	  fromSocketid: string;
	  inCallWith: number;
	}) => handleCallingReconnected(data)
      );
      socketContext.socket.on(
	"calling_reconnected_answer",
	(data: {
	  key: string;
	  fromUser: number;
	  fromSocketid: string;
	  inCallWith: number;
	  inCallWithSocketid: string;
	}) => handleCallingReconnectedAnswer(data)
      );
    }
    return () => {
      if (socketContext.socket) {
	socketContext.socket.off("start_calling");
	socketContext.socket.off("end_calling");
	socketContext.socket.off("client_end_calling");
	socketContext.socket.off("accept_calling");
	socketContext.socket.off("signaling_icecandidate");
	socketContext.socket.off("signaling_offer");
	socketContext.socket.off("signaling_answer");
	socketContext.socket.off("screenshare_offer");
	socketContext.socket.off("screenshare_answer");
	socketContext.socket.off("screenshare_renegotiation_offer");
	socketContext.socket.off("screenshare_renegotiation_answer");
	socketContext.socket.off("screenshare_close");
	socketContext.socket.off("screenshare_icecandidate");
	socketContext.socket.off("hangup");
	socketContext.socket.off("renegotiation_offer");
	socketContext.socket.off("renegotiation_answer");
	socketContext.socket.off("calling_reconnected");
	socketContext.socket.off("calling_reconnected_answer");
      }
    };
  }, [socketContext.socket,
      userContext.loggedInUserId,
      remoteSocketid,
      localSocketid,
      inCallWith,
      key,
      peerConnection,
      remoteStream,
      localStream,
      icecandidates,
      screensharePeerConnection,
      remoteScreenshareStream,
      localScreenshareStream,
      screenshareIcecandidates]);

  return (
    <CallContext.Provider
      value={{
	/* ~State~ */
	peerConnection, setPeerConnection,
	remoteSocketid, setRemoteSocketid,
	localSocketid, setLocalSocketid,
	callingType, setCallingType,
	calling, setCalling,
	inCallWith, setInCallWith,
	callWindowLayout, setCallWindowLayout,
	mic, setMic,
	showCam, setShowCam,
	showScreen, setShowScreen,
	remoteMic, setRemoteMic,
	remoteShowCam, setRemoteShowCam,
	remoteShowScreen, setRemoteShowScreen,
	localStream, setLocalStream,
	remoteStream, setRemoteStream,
	remoteStreamVolume, setRemoteStreamVolume,
	screensharePeerConnection, setScreensharePeerConnection,
	localScreenshareStream, setLocalScreenshareStream,
	remoteScreenshareStream, setRemoteScreenshareStream,
	remoteStreamAudio, setRemoteStreamAudio,
	remoteStreamAudioContext, setRemoteStreamAudioContext,
	remoteStreamGainNode, setRemoteStreamGainNode,
	/* ~Methods~ */
	resetState,
	reset,
	startCalling,
	endCalling,
	acceptCalling,
	signalingStartCall,
	startScreenshareSignaling,
	startScreenshareRenegotiation,
	screenshareClose,
	hangup,
	startRenegotiation,
	callingReconnected,
      }}>
      { props.children }
    </CallContext.Provider>
  );
};

export default CallContextProvider;

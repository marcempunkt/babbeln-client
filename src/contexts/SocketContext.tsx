import React, { useState, useEffect, useContext, createContext } from "react";
import io, { Socket } from "socket.io-client";
import logger from "../utils/logger";

import { SocketContextValue, AppContextValue } from "../ts/types/contextvalue_types";
import { useAppContext } from "./AppContext";
import { ServerToClientEvents, ClientToServerEvents, SocketState } from "../ts/types/socket_types";

export const SocketContext = createContext<SocketContextValue | undefined>(undefined);

export const useSocketContext = () => {
  /**
   * Consume SocketContext with error handling
   * @public
   *
   * returns {Context}    SocketContext
   */
  const context = useContext(SocketContext);
  if (context === undefined) {
    throw Error( "SocketContext is undefined! Are you consuming SocketContext outside of the SocketContextProvider?" );
  }
  return context;
};

interface Props {
  children: JSX.Element;
}

const SocketContextProvider: React.FC<Props> = (props: Props) => {

  const appContext: AppContextValue = useAppContext();

  const [socket, setSocket] = useState<Socket<ServerToClientEvents, ClientToServerEvents> | undefined>(undefined);
  const [state, setState] = useState<SocketState>(SocketState.Disconnected);
  const [reconnected, setReconnected] = useState<number>(0);

  useEffect(() => { logger.log.magenta("SocketContext", `State changed: ${state}`); }, [state]);

  const reset = () => {
    /**
     * Cleanup the SocketContext State to reset it for the next User to login
     * @public 
     *
     * @returns  {boolean}  true
     */
    if (socket) {
      socket.disconnect();
      setSocket(undefined);
    }
    // setState(SocketState.Disconnected) <- no need to call this because of socket.disconnect()
    setReconnected(0);
  };

  const createSocketConnection: (auth: string) => boolean = (auth) => {
    /**
     * create a connection with the socket server
     * @public
     *
     * @param  {String}  auth: Authentication token
     */
    switch (auth) {
      case null:
        console.error("auth is null");
	if (socket) {
          socket.disconnect();
	}
        console.error("createSocketConnection: ", false);
	return false;
      default:
        /* create socket */
	setSocket((_oldState) => {
	  const socketState: Socket<ServerToClientEvents, ClientToServerEvents> = io(
	    appContext.API_URL,
	    {
              query: { "auth": `${auth}` },
              autoConnect: false,
            }
	  );
          /* if it was connected, disconnect first */
	  socketState.disconnect();
	  socketState.connect();
	  return socketState;
	});
	return true;
    }
  };

  const manuallyDisconnect: () => boolean = () => {
    /**
     * manually disconnect from the socket server
     * @public
     *
     * @returns  {boolean}  true: successfully disconnected | false: the opposite ^^
     */
    if (socket) {
      socket.disconnect(); 
      return true;
    } 
    return false;
  };

  const maybeReconnect: () => boolean = () => {
    /**
     * Manually connect to the socket server in case the socket is disconnected
     * this has to be done every time the client tries to emit a socket event
     * but isn't connected to the socket server due to many possible reasons
     * @public
     */
    if (socket && socket.disconnected && !socket.connected) {
      // console.log("socket.connect()");
      socket.connect();
      return true;
    }
    // console.log("!socket.connect()");
    return false;
  };

  useEffect(() => {
    /**
     * Connect & Disconnect are build-in events from socket.io
     * depending on the "current" state update the socketcontext state variabel
     * to update the ui respectively
     * @effect
     * 
     * @dependency { Socket | null }  socket
     */
    if (socket) {
      socket.on("connect", () => setState(SocketState.Connected));
      socket.on("disconnect", () => setState(SocketState.Disconnected));
    }
  }, [socket]);

  return (
    <SocketContext.Provider
      value={{
	/* ~State~ */
	socket, setSocket,
	state, setState,
	reconnected, setReconnected,
	/* ~Methods~ */
	reset,
	createSocketConnection,
	manuallyDisconnect,
	maybeReconnect,
      }}>
      {props.children}
    </SocketContext.Provider>
  );
};

export default SocketContextProvider;

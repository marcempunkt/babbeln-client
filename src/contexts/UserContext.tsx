import React, { useState, useEffect, useContext, createContext } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../utils/logger";

import { TinyInt } from "../ts/types/mariadb_types";
import { CancelToken, Response } from "../ts/types/axios_types";
import { useSocketContext } from "./SocketContext";
import { useAppContext } from "./AppContext";
import { UserContextValue, AppContextValue, SocketContextValue } from "../ts/types/contextvalue_types";
import { Status, UserWithoutPassword } from "../ts/types/user_types";

export const UserContext = createContext<UserContextValue | undefined>(undefined);

export const useUserContext = () => {
    /**
     * Consume SocketContext with error handling
     * @public
     * * returns {Context}    SocketContext
     */
    const context = useContext(UserContext);
    if (context === undefined) {
        throw Error( "UserContext is undefined! Are you consuming UserContext outside of the UserContextProvider?" );
    }
    return context;
};

interface Props {
    children: JSX.Element;
}

const UserContextProvider: React.FC<Props> = (props: Props) => {

    const socketContext: SocketContextValue = useSocketContext();
    const appContext: AppContextValue = useAppContext();

    const [token, setToken] = useState<string | null>(null);

    useEffect(() => {
        /**
         * Fetch user data & create a socket connection with the new token
         * Info: this side-effect gets called on its first render and then
         * everytime the token updates!
         * @effect
         * 
         * @dependency  {string}  authentication token
         */
        fetchUser();
        /* Create Connection with the Socket Server */
        if (token) socketContext.createSocketConnection(token);
    }, [token]);

    const [loggedInUserId, setLoggedInUserId] = useState<number>(0);
    const [loggedInUsername, setLoggedInUsername] = useState<string>("");
    const [status, setStatus] = useState<Status | null>(null);
    const [registerDate, setRegisterDate] = useState<number>(0);
    const [isActive, setIsActive] = useState<TinyInt>(TinyInt.FALSE);
    const [email, setEmail] = useState<string>("");
    /* avatar is the url to the profile pic of the currently logged in user
     * smthng like => https://URL/api/user/profile/get-avatar/:userId?t=${Date().now()} */
    const [avatar, setAvatar] = useState<string>(`${appContext.API_URL}/users/get/avatar/0`); 

    const reset = () => {
        /**
         * reset the whole usercontext state
         * @public
         */
        setToken(null);
        setLoggedInUserId(0);
        setLoggedInUsername("");
        setStatus(null);
        setRegisterDate(0);
        setIsActive(TinyInt.FALSE);
        setEmail("");
        setAvatar(`${appContext.API_URL}/users/get/avatar/0`);
    };

    const fetchUser = () => {
        /**
         * Fetch all messages associated with the user
         * @public
         */
        if (!token) return logger.error.magenta("UserContext", "No Authentication Token was found.");

        // TODO FIXME is findby token really safe???
        axios.get(`${appContext.API_URL}/users/get/user/token/${token}`)
	     .then((res: AxiosResponse<UserWithoutPassword>) => {
	         const loggedInUserId: number = res.data.id;
	         /* Save user data into the state */
	         setLoggedInUserId(res.data.id);
	         setLoggedInUsername(res.data.username);
	         setStatus(res.data.status);
	         setIsActive((res.data.is_active ? TinyInt.TRUE : TinyInt.FALSE));
	         setRegisterDate(res.data.register_date);
	         setEmail(res.data.email);
	         setAvatar(`${appContext.API_URL}/users/get/avatar/${loggedInUserId}?t=${new Date().getTime()}`);
	         logger.log.magenta("UserContext", "Fetched User Data.");
	     }).catch((err: AxiosError) => console.error(err));
    };

    const getUsernameById: (userId: number, cancelToken: CancelToken) => Promise<string> = (userId, cancelToken) => {
        /**
         * Get the username by userid
         * @public
         *
         * @param  {number}     userId           id of a user
         * @param  {any}        cancelToken      axios CancelToken
         *
         * returns {Promise}    either username {String} or error
         */
        return new Promise<string>((resolve, reject) => {
            axios
                .get(`${appContext.API_URL}/users/get/user/id/${userId}`, {
                    cancelToken,
                })
                .then((res: AxiosResponse<{ username: string }>) => {
                    resolve(res.data.username);
                })
                .catch((err: AxiosError) => {
                    reject(err);
                });
        });
    };

    const getRegisterDateById: (userId: number, cancelToken: CancelToken) => Promise<number> = (userId, cancelToken) => {
        /**
         * Get the date a user signed up
         * @public
         *
         * @param  {number}     userId           id of a user
         * @param  {any}        cancelToken      axios CancelToken
         *
         * returns {Promise}    either date {number} or error
         */
        return new Promise<number>((resolve, reject) => {
            axios
                .get(`${appContext.API_URL}/users/get/registerdate/${userId}`, {
                    cancelToken,
                })
                .then((res: AxiosResponse<number>) => {
                    resolve(res.data);
                })
                .catch((err: AxiosError<Response>) => {
                    reject(err);
                });
        });
    };

    const updateAvatar: () => void = () => {
        /**
         * update the url of your avatar pic 
         * @public
         */
        setAvatar(`${appContext.API_URL}/users/get/avatar/${loggedInUserId}?t=${new Date().getTime()}`);
        socketContext.maybeReconnect();
        if (socketContext.socket) {
            logger.log.magenta("UserContext", "Socket event emitted update_avatar.");
            socketContext.socket.emit("update_avatar", loggedInUserId); 
        }
    };

    const updateUsername: (newUsername: string) => void = (newUsername) => {
        /**
         * update your username inside the client's state and then emit a socket event
         * @public
         * 
         * @params  {string}   newUsername
         */
        // TODO check if newUsername is a correct username
        setLoggedInUsername(newUsername);
        socketContext.maybeReconnect();
        if (socketContext.socket) {
            logger.log.magenta("UserContext", "Socket event emitted update_username.");
            socketContext.socket.emit("update_username", loggedInUserId, newUsername); 
        }
    };

    const updateStatus = (status: Status) => {
        setStatus(status);
        socketContext.maybeReconnect();
        if (socketContext.socket) {
            logger.log.magenta("UserContext", "Socket event emitted update_status.");
            socketContext.socket.emit("update_status", loggedInUserId, status);
        }
    };

    const handleStatusClasses: (onlineStatus: Status | null) => string = (onlineStatus) => {
        /**
         * returns the correct classes for the online status of a user
         * @public
         * 
         * @params  {Status}  onlineStatus
         * 
         * @returns {string}  css classes
         */
        let classes: string = "";
        /* Check online status */
        switch (onlineStatus) {
            case Status.ON:
	        return classes += " status--online";
            case Status.OFF:
	        return classes += " status--offline";
            case Status.AWAY:
	        return classes += " status--away";
            case Status.DND:
	        return classes += " status--dnd";
            default:
	        return classes += " status--offline";
        }
        
        return classes;
    };

    useEffect(() => {
        /**
         * Socket EventListeners
         * @effect
         */
        if (socketContext.socket) {
            socketContext.socket.on(
	        "client_update_avatar",
	        () => setAvatar(`${appContext.API_URL}/users/get/avatar/${loggedInUserId}?t=${new Date().getTime()}`)
            );
            socketContext.socket.on(
	        "client_update_status",
	        (newStatus: Status) => setStatus(newStatus)
            );
            socketContext.socket.on(
	        "client_update_username",
	        (newUsername: string) => setLoggedInUsername(newUsername)
            )
        }
        return () => {
            if (socketContext.socket) {
	        socketContext.socket.off("client_update_avatar");
	        socketContext.socket.off("client_update_status");
	        socketContext.socket.off("client_update_username");
            }
        };
    }, [socketContext.socket, loggedInUserId, status]);

    return (
        <UserContext.Provider
            value={{
	        /* ~State~ */
	        token, setToken,
	        loggedInUserId, setLoggedInUserId,
	        loggedInUsername, setLoggedInUsername,
	        status, setStatus,
	        registerDate, setRegisterDate,
	        isActive, setIsActive,
	        email, setEmail,
	        avatar, setAvatar,
	        /* ~Methods~ */
	        reset,
	        fetchUser,
	        getUsernameById,
	        getRegisterDateById,
	        updateAvatar,
	        updateUsername,
	        updateStatus,
	        handleStatusClasses,
            }}>
            {props.children}
        </UserContext.Provider>
    );

};

export default UserContextProvider;

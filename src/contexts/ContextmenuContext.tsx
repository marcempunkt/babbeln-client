import React, { useState, useContext, createContext } from "react";

import {
  ContextmenuContextValue,
  ModalContextValue,
  FriendsContextValue,
  MessageContextValue,
  AppContextValue } from "../ts/types/contextvalue_types";

import { useModalContext } from "./ModalContext";
import { useFriendsContext } from "./FriendsContext";
import { useMessageContext } from "./MessageContext";
import { useAppContext } from "./AppContext";

export const ContextmenuContext = createContext<ContextmenuContextValue | undefined>(undefined);

export const useContextmenuContext = () => {
  /**
   * Consume ContextmenuContext with error handling
   * @public
   *
   * returns {Context}    ContextmenuContext
   */
  const context = useContext(ContextmenuContext);
  if (context === undefined) {
    throw Error( "ContextmenuContext is undefined! Are you consuming ContextmenuContext outside of the ContextmenuContextProvider?" );
  }
  return context;
}

interface Props {
  children: Array<JSX.Element>;
}

const ContextmenuContextProvider: React.FC<Props> = (props: Props) => {

  const modalContext: ModalContextValue = useModalContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const messageContext: MessageContextValue = useMessageContext();
  const appContext: AppContextValue = useAppContext();

  const [posX, setPosX] = useState<number>(0);
  const [posY, setPosY] = useState<number>(0);
  const [showContextmenu, setShowContextmenu] = useState<boolean>(false);
  const [components, setComponents] = useState<Array<JSX.Element>>([]);

  const create = (x: number, y: number) => {
    /**
     * spawns Contextmenu on (x|y)
     * @public
     *
     * @param  {number}        x
     * @param  {number}        y
     */

    /* Check Window boundaries */
    // TODO bedenke die Contextmenu größe mit ein
    if (x > window.innerWidth) {
      setPosX(window.innerWidth);
    } else if (x < 0) {
      setPosX(0);
    } else {
      setPosX(x);
    }

    if (y > window.innerHeight) {
      setPosY(window.innerHeight);
    } else if (y < 0) {
      setPosY(0);
    } else {
      setPosY(y);
    }

    setShowContextmenu(true);
  }

  /*
   * Functions that are frequently used inside the Contextmenu
   * therefore being stored inside this shared state
   */

  const handleRemoveFriend = (friendId: number) => {
    /**
     * Removes a friend from your friendslist 
     * @public
     *
     * @param  {number}  friendId  userId of your friend
     */
    
    modalContext.setShowRemoveFriendModal(true);
    friendsContext.setRemoveFriendId(friendId);
  }

  const handleShowProfile = (userId: number) => {
    /**
     * Shows Profile of a user
     * @public
     *
     * @param  {number} userId
     */
    
    modalContext.setShowProfileModal(true);
    modalContext.setShowProfileId(userId);
  }

  const handleStartChat = (userId: number) => {
    /**
     * Opens the chat view from the Friends Component
     * @private
     *
     * @param  {number}  userId
     */
    
    /* FriendItem */
    if (appContext.showFriends) {
      appContext.setShowFriends(false);
    }
    messageContext.setShowMessagesFromUserId(userId)
  };

  return(
    <ContextmenuContext.Provider value={{
      posX, setPosX,
      posY, setPosY,
      showContextmenu, setShowContextmenu,
      components, setComponents,
      create,
      handleStartChat,
      handleRemoveFriend,
      handleShowProfile,
    }}>
      { props.children }
    </ContextmenuContext.Provider>
  );
}

export default ContextmenuContextProvider;

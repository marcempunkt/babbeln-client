import React, { useState, useEffect, createContext, useContext, MouseEvent } from "react";
import axios from "axios";
import moment from "moment-timezone";

import { AppContextValue } from "../ts/types/contextvalue_types";
import { Theme } from "../ts/types/config_types";
import { ElectronAPI } from "../ts/types/electron_types";

export const AppContext = createContext<AppContextValue | undefined>(undefined);

export const useAppContext = () => {
    /**
     * Consume AppContext with error handling
     * @public
     *
     * returns {Context}    AppContext
     */
    const context = useContext(AppContext);
    if (context === undefined) {
        throw Error( "AppContext is undefined! Are you consuming AppContext outside of the AppContextProvider?" );
    }
    return context;
};

interface Props {
    children: JSX.Element;
}

const AppContextProvider: React.FC<Props> = (props: Props) => {
    /**
     * The AppContext is for all storing functions & state that is essential for the whole App
     * but it isn't necessary to create a whole seperate Context for it
     * @component
     */
    const API_URL: string = (process.env.NODE_ENV === "production") ? `https://server.babbeln.app` : `http://localhost:3001`;

    /* const [apiClient, setApiClient] = useState<string>(() => {
     *     const client = axios.create({
     *         baseUrl: API_URL
     *     });

     *     client.interceptors.request.use((config: string) => {
     *         const token = localStorage.getItem('token');
     *         config.headers.Authorization =  token ? `Bearer ${token}` : '';
     *         return config;
     *     });
     * }()); */

    const [timezone, setTimezone] = useState<string>("");
    const [theme, setTheme] = useState<Theme>(Theme.NORD_DARK);
    const [showSettings, setShowSettings] = useState<boolean>(false);  /* Settings Panel */
    const [showFriends, setShowFriends] = useState<boolean>(false);    /* Show Friends Block */
    const [showDashboard, setShowDashboard] = useState<boolean>(true); /* default */
    const [showTimeline, setShowTimeline] = useState<boolean>(false);  /* facebook/twitter like thing */

    const reset = () => {
        /**
         * reset the whole appcontext state
         * @public
         */
        setTimezone("");
        setTheme(Theme.NORD_DARK);
        setShowSettings(false);
        setShowFriends(false);
        setShowDashboard(true);
        setShowTimeline(false);
    };

    const handleDate = (milisec: number) => (
        /*
         * Returns a string of the date by passing the milisec passed after UNIX_EPOCH
         * TODO: add support for different date formats
         * @public
         * 
         * returns  {string}  date
         */
        new Date(milisec).toLocaleTimeString("de-De", { hour: "2-digit", minute: "2-digit" })
    );

    const getFileUrls: (fileList: FileList) => Array<Promise<string>> = (fileList) => {
        /**
         * 
         * @private
         * 
         */
        const getBase64: (file: File) => Promise<string> = (file) => {
            const reader: FileReader = new FileReader();
            return new Promise<string>((resolve) => {
	        reader.onload = (event: ProgressEvent<FileReader>) => {
	            if (!event) return resolve("");
	            if (!event.target) return resolve("");
	            const result: string | ArrayBuffer | null = event.target.result;
	            if (typeof result === "string") return resolve(result);
	            return resolve("");
	        }
	        reader.readAsDataURL(file);
            });
        };

        const fileUrls: Array<Promise<string>> = [...fileList].map((file: File) => getBase64(file));
        // const filteredFileUrls: Array<string> = fileUrls.filter((fileUrl: string) => fileUrl !== "")
        // return filteredFileUrls;
        return fileUrls;
    };

    const isElectron: () => boolean = () => {
        /**
         * This fn is copy&pasted from npm package 'is-electron' by Cheton Wu
         * License: MIT
         * Thank you Cheton, I hope you don't mind!
         * @public
         * 
         * @returns  { boolean }  isElectron
         */
        /* Renderer process */
        if (typeof window !== 'undefined' && typeof window.process === 'object' && window.process.type === 'renderer') {
            return true;
        }
        /* Main process */
        if (typeof process !== 'undefined' && typeof process.versions === 'object' && !!process.versions.electron) {
            return true;
        }
        /* Detect the user agent when the `nodeIntegration` option is set to false */
        if (typeof navigator === 'object' && typeof navigator.userAgent === 'string' && navigator.userAgent.indexOf('Electron') >= 0) {
            return true;
        }
        return false;
    };

    const linkify: (content: string) => Array<string | JSX.Element> = (content) => {
        /**
         * Check if content has a link in it
         * Split msgContent everywhere where there is an url (space doesn't matter)
         * replace the url parts of that array with jsx (a element)
         * spread it into the returned jsx
         * @public
         * 
         * @param    {string}                       content  the content of a msg/post/comment/...
         * 
         * @returns  {Array<string | JSX.Element>}  for example: ["this website is awesome ", <a>url</a>, " !!!!"]
         */
        const regex: RegExp = /((http|https):\/\/\S+)/gi;

        if (!(new RegExp(regex).test(content))) return [content];

        /* fn for clicking on a link in electron */
        const handleLinkClick = (e: MouseEvent<HTMLElement>, url: string) => {
            if (!isElectron()) return;
            e.preventDefault();
            window.electron.shell.openExternal(url);
        };

        /**
         * There is a url the content of this msg
         * Create a new Array of the parts of the msg, where
         * the url string is replaced with JSX (<a> HTML Element)
         */
        let match: RegExpExecArray | null;
        const ranges: Array<number[]> = []; /* Array of Tupels[startUrl, endUrl] */
        /* get the ranges where the url begins & ends */
        do {
            match = regex.exec(content);
            if (!match) break;
            const range: Array<number> = [match.index, regex.lastIndex]; 
            ranges.push(range);
        } while (match);
        /* Construct a new Array out with the knowledge of the ranges of the urls*/
        let newContent: Array<string | JSX.Element> = [];
        let currentIndex: number = 0; // index of the msgContent indicating how far the map below has reached

        ranges.map((range: Array<number>) => {
            newContent.push(content.slice(currentIndex, range[0])); // add everything before the url
            currentIndex = range[1]; // in the next cycle we should start at the end of this cycle 
            // const url: string = punycode.toASCII(content.slice(range[0], range[1]));
            const url: string = content.slice(range[0], range[1]);
            const link: JSX.Element = <a href={ url }
				         onClick={ (e: MouseEvent<HTMLElement>) => handleLinkClick(e, url) }
				         rel="noopeneer noreferrer"
				         target="_blank">{ url }</a>;
            newContent.push(link);
        });
        /* add the rest of the msg */
        newContent.push(content.substring(ranges[ranges.length - 1][1])); 
        return newContent;
    };

    useEffect(() => {
        /* Initialize theme */
        const theme: string | null = localStorage.getItem("theme");
        switch (theme) {
            case "light":
	        return setTheme(Theme.LIGHT);
            case "dark":
	        return setTheme(Theme.DARK);
            case "nord-light":	
	        return setTheme(Theme.NORD_LIGHT);
            case "nord-dark":
	        return setTheme(Theme.NORD_DARK);
            default:
	        return setTheme(Theme.DARK);
        }
    }, []);

    useEffect(() => {
        /* Update theme */
        const root: HTMLDivElement = document.querySelector("#root")!;
        const updateTheme: (theme: Theme) => void = (theme) => {
            root.classList.remove("light", "dark", "nord-light", "nord-dark");
            root.classList.add(theme);
        };
        localStorage.setItem("theme", theme);
        updateTheme(theme);
    }, [theme]);

    useEffect(() => {
        /* guess the timezone of the client */
        setTimezone(moment.tz.guess(true));
    }, []);

    return(
        <AppContext.Provider value={ {
            API_URL,
            /* ~State~ */
            timezone, setTimezone,
            theme, setTheme,
            showSettings, setShowSettings,
            showFriends, setShowFriends,
            showDashboard, setShowDashboard,
            showTimeline, setShowTimeline,
            /* ~Methods~ */
            reset,
            handleDate,
            getFileUrls,
            isElectron,
            linkify,
        } }>
            { props.children }
        </AppContext.Provider>
    );
}

export default AppContextProvider;

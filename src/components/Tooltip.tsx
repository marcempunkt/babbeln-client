import React from "react";
import "./Tooltip.scss";

export enum TooltipPlacement {
  TOP = "top",
  BOTTOM = "bottom",
  LEFT = "left",
  RIGHT = "right",
}

interface Props {
  title: string;
  placement?: TooltipPlacement;
  children: JSX.Element;
}

const Tooltip: React.FC<Props> = (props: Props) => {
  /**
   * A wrapper for displaying a tooltip on hover
   * @component
   */
  return(
    <div className="tooltip">
      { props.children }
      <span className="tooltip__description">{ props.title }</span>
      <span className="tooltip__arrow" />
    </div>
  )
};

Tooltip.defaultProps = {
  placement: TooltipPlacement.BOTTOM,
};

export default Tooltip;

import React, { MouseEvent } from "react";

import { ReactComponent as UserCheck } from "../../assets/feathericons/user-check.svg";
import { ReactComponent as UserMinus } from "../../assets/feathericons/user-minus.svg";

import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useModalContext } from "../../contexts/ModalContext";
import { ContextmenuContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

import ContextmenuItem from "../contextmenu/ContextmenuItem";
import GenericUserlistItem from "../GenericUserlistItem";
import Tooltip from "../Tooltip";

interface Props {
  friendId: number;
  friendName: string;
}

const BlockedItem: React.FC<Props> = (props: Props) => {

  const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
  const modalContext: ModalContextValue = useModalContext();

  const handleUnblock = () => {
    modalContext.setBlockOrUnblockUserId(props.friendId);
    modalContext.setShowUnblockUserModal(true);
  };

  const handleRemove = () => contextmenuContext.handleRemoveFriend(props.friendId);

  const unblockMenuItem = <ContextmenuItem
			    key={ 1 }
			    icon={ <UserCheck /> }
			    description="Unblock"
			    handleClick={ handleUnblock }/>;

  const removeFriendMenuItem = <ContextmenuItem
                                 key={ 2 }
                                 icon={ <UserMinus /> }
                                 description="Remove Friend"
                                 handleClick={ handleRemove }/>;

  const handleContextmenu = (e: MouseEvent<HTMLLIElement>) => {
    e.preventDefault();

    contextmenuContext.create(e.pageX, e.pageY);

    contextmenuContext.setComponents([
      unblockMenuItem,
      removeFriendMenuItem
   ]);
  };

  return(
    <GenericUserlistItem
      onContextmenu={ handleContextmenu }
      userId={ props.friendId }
      showActions={ true }
      actions={ [
	<Tooltip key={ 1 } title="Unblock">
	  <UserCheck onClick={ handleUnblock } className="useritem__actions__green" />
	</Tooltip>,
	<Tooltip key={ 2 } title="Remove">
	  <UserMinus onClick={ handleRemove } className="useritem__actions__red" />
	</Tooltip>
      ] } />
  );
}

export default BlockedItem;

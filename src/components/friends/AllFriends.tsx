import React from "react";

import { useFriendsContext } from "../../contexts/FriendsContext";
import { FriendsContextValue } from "../../ts/types/contextvalue_types";
import { Friend, Status } from "../../ts/types/friends_types";
import { TinyInt } from "../../ts/types/mariadb_types";

import FriendItem from "./FriendItem";

interface Props {
  showOnlyOnline?: boolean;
}

const AllFriends: React.FC<Props> = (props: Props) => {
  /**
   * Shows a list of all friend of the currently logged in user
   * @component
   */

  const friendsContext: FriendsContextValue = useFriendsContext();

  const getOnlineFriends = () => {
    /** 
     * Get all friends that are online 
     * @private
     */
    const allOnlineFriends: Array<Friend> = friendsContext.friends.filter(
      (friend: Friend) => friend.status === Status.ON || friend.status === Status.AWAY || friend.status === Status.DND 
    );
    return allOnlineFriends;
  };

  const getAllFriends = () => {
    /**
     * Get all friend that aren't blocked
     * @private
     */
    const allFriends: Array<Friend> = friendsContext.friends.filter(
      (friend: Friend) => friend.blocked === TinyInt.FALSE
    );
    return allFriends;
  }

  return(
    <ul className="userlist">
      {
	(props.showOnlyOnline) ?
	/* Render only friends that are online */
	getOnlineFriends().map((friend: Friend) => {
	  return(
	    <FriendItem
	      key={ friend.friendId }
	      friendId={ friend.friendId }
	      friendName={ friend.friendName }
	      status={ friend.status }
	    />
	  );
	})
	:
	/* Render all friends (-blocked) */
	getAllFriends().map((friend: Friend) => {
	  return(
	    <FriendItem
	      key={ friend.friendId }
	      friendId={ friend.friendId }
	      friendName={ friend.friendName }
	      status={ friend.status }
	    />
	  );
	})
      }
    </ul>
  );
};

export default AllFriends;

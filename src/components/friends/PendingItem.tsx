import React, { useState, useEffect, MouseEvent } from "react";
import axios from "axios";
import "./PendingItem.scss";

import { ReactComponent as Check } from "../../assets/feathericons/check.svg";
import { ReactComponent as X } from "../../assets/feathericons/x.svg";
import { ReactComponent as User } from "../../assets/feathericons/user.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useUserContext } from "../../contexts/UserContext";
import { useFriendrequestsContext } from "../../contexts/FriendrequestsContext";
import { AppContextValue,
	 ContextmenuContextValue,
	 UserContextValue,
	 FriendrequestsContextValue } from "../../ts/types/contextvalue_types";

import ContextmenuItem from "../contextmenu/ContextmenuItem";

interface Props {
  pendingId: number;
  fromUser: number;
  toUser: number;
}

const PendingItem: React.FC<Props> = (props: Props) => {

  const appContext: AppContextValue = useAppContext();
  const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
  const userContext: UserContextValue = useUserContext();
  const friendrequestsContext: FriendrequestsContextValue = useFriendrequestsContext();

  const [username, setUsername] = useState<string>("unknown");

  const userId: number = (userContext.loggedInUserId === props.fromUser) ? props.toUser : props.fromUser;

  useEffect(() => {
    /**
     * Get username
     * @effect
     */
    /* Get who sent this pending friendrequest */
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    userContext.getUsernameById(userId, source.token)
		 .then((username: string) => setUsername(username))
		 .catch(() => setUsername("unknown"));

    return () => source.cancel();
  }, [props.fromUser, props.toUser]);

  const handleShowProfile = () => {
    const userId = (userContext.loggedInUserId === props.fromUser) ? props.toUser : props.fromUser;
    return contextmenuContext.handleShowProfile(userId);
  };

  const cancelRequestMenuItem = <ContextmenuItem
				  key={ 1 }
				  icon={ <X /> }
				  description="Cancel Request"
				  handleClick={ () => friendrequestsContext.declineFriendrequest(props.pendingId) }/>;

  const showProfileMenuItem = <ContextmenuItem
				key={ 2 }
				icon={ <User /> }
				description="Show Profile"
				handleClick={ handleShowProfile }/>;

  const acceptRequestMenuItem = <ContextmenuItem
				  key={ 3 }
				  icon={ <Check /> }
				  description="Accept"
				  handleClick={ () => friendrequestsContext.acceptFriendrequest(props.pendingId) }/>;

  const declineRequestMenuItem = <ContextmenuItem
				   key={ 4 }
				   icon={ <X /> }
				   description="Decline"
				   handleClick={ () => friendrequestsContext.declineFriendrequest(props.pendingId) }/>;

  const handleContextmenu = (e: MouseEvent<HTMLLIElement>) => {
    e.preventDefault();

    contextmenuContext.create(e.pageX, e.pageY);

    const iSentTheReq: boolean = (userContext.loggedInUserId === props.fromUser);

    if (iSentTheReq) {
      contextmenuContext.setComponents([
	cancelRequestMenuItem,
	showProfileMenuItem
     ])
    } else {
      contextmenuContext.setComponents([
	acceptRequestMenuItem,
	declineRequestMenuItem,
	showProfileMenuItem,
     ]);
    }
  }

  return(
    <li className={ "useritem useritem--always-show-actions" }
	onContextMenu={ handleContextmenu } >

      <img
        className={ "useritem__icon status--offline" }
        alt={ username }
        src={ `${appContext.API_URL}/users/get/avatar/${userId}` } />

      <span className="useritem__name">{ username }</span>

      <div className="useritem__actions">
	{
	  (userContext.loggedInUserId === props.fromUser) ?
	  <span key={ 0 } className="useritem__actions__placeholder">pending...</span>
	  :
	  <React.Fragment>
	    <Check key={ 1 } onClick={ () => friendrequestsContext.acceptFriendrequest(props.pendingId) } className="useritem__actions__green" />
	    <X key={ 2 } onClick={ () => friendrequestsContext.declineFriendrequest(props.pendingId) } className="useritem__actions__red" />
	  </React.Fragment>
	}
      </div>

    </li>
  );
};

export default PendingItem;

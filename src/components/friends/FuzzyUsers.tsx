import React, { useState, useEffect, MouseEvent } from "react";
import "./FuzzyUsers.scss";

import { ReactComponent as UserPlus } from "../../assets/feathericons/user-plus.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useFriendrequestsContext } from "../../contexts/FriendrequestsContext";
import { useUserContext } from "../../contexts/UserContext";
import { AppContextValue,
         FriendsContextValue,
         FriendrequestsContextValue,
         UserContextValue } from "../../ts/types/contextvalue_types";
import { Friend } from "../../ts/types/friends_types";
import { FuzzyUser } from "../../ts/types/user_types";

interface Props {
  users: Array<FuzzyUser>;
}

const FuzzyUsers: React.FC<Props> = (props: Props) => {

  const appContext: AppContextValue = useAppContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const friendrequestsContext: FriendrequestsContextValue = useFriendrequestsContext();
  const userContext: UserContextValue = useUserContext();

  const [users, setUser] = useState<Array<FuzzyUser>>([]);

  const handleAddClick = (e: MouseEvent<SVGSVGElement>, userId: number) => {
    e.stopPropagation();
    friendrequestsContext.sendFriendrequest(userId);
  }

  useEffect(() => {
    /**
     * Filter all friends out of the 'fuzzy_users'
     * @effect
     */
    const allUseridsOfYourFriends: Array<number> = friendsContext.friends.map((friend: Friend) => friend.friendId);
    // const allUseridsOfPendings: Array<number> = friendsContext.pendings.map((pending: Pending) => pending.pendingId);

    const filteredUsers: Array<FuzzyUser> = props.users.filter((user: FuzzyUser) => {
      /* filter all your friends */
      if (allUseridsOfYourFriends.includes(user.id)) return false;
      /* filter yourself out */
      if (user.id === userContext.loggedInUserId) return false;
      return true;
    });

    setUser(filteredUsers);
  }, [props.users]);

  return(
    <ul className="fuzzyusers">
      {
	(props.users.length) ?
	users.map((user: FuzzyUser) => {
	  return(
	    <li key={ user.id } className="fuzzyusers__user">

	      <img className="fuzzyusers__user__avatar status--offline" src={ `${appContext.API_URL}/users/get/avatar/${user.id}` } />

	      <span className="fuzzyusers__user__name">{ user.username }</span>

	      <div className="fuzzyusers__user__actions">
		{/*
		    <UserPlus className="fuzzyusers__user__actions__default" onClick={ (e: MouseEvent<SVGSVGElement>) => handleAddClick(e, user.id) } />
		    <UserPlus className="fuzzyusers__user__actions__red" onClick={ (e: MouseEvent<SVGSVGElement>) => handleAddClick(e, user.id) } />
		  */}
		<UserPlus className="fuzzyusers__user__actions__green" onClick={ (e: MouseEvent<SVGSVGElement>) => handleAddClick(e, user.id) } />
	      </div>

	    </li>
	  )
	})
	:
	"No Users found."
      }
    </ul>
  );
};

export default FuzzyUsers;

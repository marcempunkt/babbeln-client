import React, { MouseEvent } from "react";
import "./FriendItem.scss";

import { ReactComponent as User } from "../../assets/feathericons/user.svg";
import { ReactComponent as UserMinus } from "../../assets/feathericons/user-minus.svg";
import { ReactComponent as Send } from "../../assets/feathericons/send.svg";
import { ReactComponent as UserX } from "../../assets/feathericons/user-x.svg";

import { useMessageContext } from "../../contexts/MessageContext";
import { useAppContext } from "../../contexts/AppContext";
import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useModalContext } from "../../contexts/ModalContext";
import { MessageContextValue, AppContextValue, ContextmenuContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

import ContextmenuItem from "../contextmenu/ContextmenuItem";
import GenericUserlistItem from "../GenericUserlistItem";

interface Props {
  friendId: number;
  friendName: string;
  status: string;
}

const FriendItem: React.FC<Props> = (props: Props) => {
  /**
   * @component
   */
  const messageContext: MessageContextValue = useMessageContext();
  const appContext: AppContextValue = useAppContext();
  const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
  const modalContext: ModalContextValue = useModalContext();

  const handleClick = () => {
    appContext.setShowFriends(false);
    messageContext.setShowMessagesFromUserId(props.friendId);
  }

  const handleBlock = () => {
    modalContext.setBlockOrUnblockUserId(props.friendId);
    modalContext.setShowBlockUserModal(true);
  };

  /* Components for Contextmenu */
  const startChatMenuItem = <ContextmenuItem
			      key={ 1 }
			      icon={ <Send /> }
			      description="Open Chat"
			      handleClick={ () => contextmenuContext.handleStartChat(props.friendId) }/>;

  const showProfileMenuItem = <ContextmenuItem
				key={ 2 }
				icon={ <User /> }
				description="Show Profile"
				handleClick={ () => contextmenuContext.handleShowProfile(props.friendId) }/>;

  const blockFriendMenuItem = <ContextmenuItem
				key={ 3 }
				icon={ <UserX /> }
				description="Block"
				handleClick={ handleBlock }/>;

  const removeFriendMenuItem = <ContextmenuItem
				 key={ 4 }
				 icon={ <UserMinus /> }
				 description="Remove Friend"
				 handleClick={ () => contextmenuContext.handleRemoveFriend(props.friendId) }/>;

  const handleContextmenu = (e: MouseEvent<HTMLLIElement>) => {
    e.preventDefault();

    contextmenuContext.create(e.pageX, e.pageY);
    
    contextmenuContext.setComponents([
      startChatMenuItem,
      showProfileMenuItem,
      blockFriendMenuItem,
      removeFriendMenuItem
   ]);
  };

  return(
    <GenericUserlistItem
      userId={ props.friendId }
      onContextmenu={ handleContextmenu }
      onClick={ handleClick } />
  );
};

export default FriendItem;

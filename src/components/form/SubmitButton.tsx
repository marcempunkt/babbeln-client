import React from "react";
import "./SubmitButton.scss";

interface Props {
  className: string;
  isDisabled: boolean;
  innerHtml: string;
}

const SubmitButton: React.FC<Props> = (props: Props) => {

  return(
    <button
      className={ ((props.className) ? props.className : "btn") + " full-width margin-top " }
      disabled={ props.isDisabled }
      type="submit">
      { props.innerHtml }
    </button>
  );
};

export default SubmitButton;

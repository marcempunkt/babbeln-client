import React, { useRef, ChangeEvent } from "react";
import "./Input.scss";

interface Props {
  htmlId: string;
  label: string;
  inputType: string;
  value: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.FC<Props> = (props: Props) => {

  const inputRef = useRef<HTMLInputElement>(null);
  const divRef = useRef<HTMLDivElement>(null);

  const handleFocus = () => {
    /* Change focus to input */
    inputRef.current!.focus(); 
    /* Apply Transition/Animation to label */
    divRef.current!.classList.add("form__input--focus");
  };

  // TODO onfocusout does not exist in div

  return(
    <React.Fragment>

      <div className="form__input" ref={ divRef } onFocus={ handleFocus } >
	<label htmlFor={ props.htmlId }>{ props.label }</label>

	<input
          ref={ inputRef }
          id={ props.htmlId }
	  type={ props.inputType }
	  value={ props.value }
	  onChange={ props.onChange } />
      </div>

    </React.Fragment>
  );
};

export default Input;

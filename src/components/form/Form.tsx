import React, { FormEvent } from "react";
import "./Form.scss";

interface Props {
  children: JSX.Element | JSX.Element[];
  title?: string;
  error?: string;
  success?: string;
  onSubmit: (e: FormEvent<HTMLFormElement>) => void;
}

const Form: React.FC<Props> = (props: Props) => {

  return(
    <form className="form" onSubmit={ props.onSubmit }>

      {
	(props.title) ?
	<h1 className="form__title">{ props.title }</h1>
	:
	null
      }

      { (props.error) ? <span className="form__error">{ props.error }</span> : null }
      { (props.success) ? <span className="form__success">{ props.success }</span> : null }

      { props.children }
    </form>
  );
};

export default Form;

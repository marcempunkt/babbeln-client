import React, { useState, useRef, FormEvent, ChangeEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import "./ChangeProfileImageModal.scss";

import { ReactComponent as FilePlus } from "../../assets/feathericons/file-plus.svg";
import { ReactComponent as FileMinus } from "../../assets/feathericons/file-minus.svg";
import { ReactComponent as Upload } from "../../assets/feathericons/upload.svg";
import { ReactComponent as AlertCircle } from "../../assets/feathericons/alert-circle.svg";

import { useModalContext } from "../../contexts/ModalContext";
import { useAppContext } from "../../contexts/AppContext";
import { useUserContext } from "../../contexts/UserContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { NotificationType } from "../../ts/types/notification_types";
import { ModalContextValue,
	 AppContextValue,
	 UserContextValue,
	 NotificationContextValue } from "../../ts/types/contextvalue_types";
import { Response } from "../../ts/types/axios_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalContent from "./ModalContent";
import CloseButton from "../../ui/CloseButton";

const ChangeProfileImageModal: React.FC = () => {

    const modalContext: ModalContextValue = useModalContext();
    const appContext: AppContextValue = useAppContext();
    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();

    const inputFile = useRef<HTMLInputElement>(null);
    const previewImage = useRef<HTMLImageElement>(null);
    const [imageUrl, setImageUrl] = useState<string>("");
    const [error, setError] = useState<string>("");
    const sizeLimit: number = 1068576;

    const handleClose = () => modalContext.setShowChangeProfileImageModal(false);

    const handleAddPic = () => {
        if (inputFile && inputFile.current) {
            inputFile.current.click()
        }
    };

    const handleFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
        if (!e.currentTarget.files) return console.error("no file selected");
        /* if no file selected do nothing
         * but for some reason console.error does not get triggered
         * and if I remove it causes a runtime error (classical programming issue...) */
        if (!e.currentTarget.files.length) return console.error("no file selected")
        /* reset error message */
        setError("");
        /* check file type */
        const imageType: string = e.currentTarget.files[0].type;
        if (!(imageType === "image/gif" ||
	      imageType === "image/png" ||
	      imageType === "image/jpeg")) {
            setError("Only PNG, JPG & GIF are allowed!");
        }
        /* check if image is not too big */
        if (e.currentTarget.files[0].size > sizeLimit) {
            setError("Image is too big!");
        }
        /* set preview image */
        const images: Array<string> = await Promise.all(appContext.getFileUrls(e.currentTarget.files));
        setImageUrl(images[0]);
    };

    const handleDrop = () => console.log("drop");
    const handleDragOver = () => console.log("dragover");

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if(!inputFile || !inputFile.current || !inputFile.current.files) return;

        const formData: FormData = new FormData();
        formData.append("avatar", inputFile.current.files[0]);
        formData.append("token", userContext.token!);

        axios.post(
            `${appContext.API_URL}/users/change/avatar`,
            formData,
            { headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": `Bearer: ${userContext.token}`,
            } }
        ).then((_res: AxiosResponse<Response>) => {
	    userContext.updateAvatar();
	    notifContext.create(NotificationType.SUCCESS, "Succesfully changed your Avatar");
	    handleClose();
	}).catch((err: AxiosError<Response>) => {
	    console.error(err);
	    if (err.response) {
	        notifContext.create(NotificationType.ERROR, err.response.data.message);
	    }
	});
    };

    return(
        <Modal>
            <CloseButton onClick={ handleClose } />
            <ModalTitle>Your new profile pic</ModalTitle>
            <ModalContent>

	        {
	            /* render preview of the new profile img or button */
	            (imageUrl)
	            ?
	            <div className="changeprofileimagemodal__preview">
	                <img className="changeprofileimagemodal__preview__image" src={ imageUrl } alt="New Profile Image" ref={ previewImage } />
	            </div>
	            :
	            <button className="btn--active btn--with-svg margin-top full-width changeprofileimagemodal__selectbtn"
		            onClick={ handleAddPic } onDragOver={ handleDragOver } onDrop={ handleDrop }>
	                <FilePlus />{ "choose or drag n' drop" }
	            </button>
	        }

	        <form onSubmit={ handleSubmit } className="margin-top">

	            {
	                (imageUrl)
	                ?
	                <button className="btn btn--with-svg margin-bottom full-width" type="button" onClick={ handleAddPic }>
	                    <FileMinus />{ "Choose another image" }
	                </button>
	                :
	                null
	            }

	            <button
	                className={ `btn${(error) ? "--error" : "--active"} btn--with-svg full-width` }
	                type="submit"
	                disabled={ (!imageUrl || error) ? true : false }>
	                {
	                    (error) ?
	                    <React.Fragment><AlertCircle />{ error }</React.Fragment>
	                    :
	                    <React.Fragment><Upload />{ "Upload" }</React.Fragment>
	                }
	            </button>
	            
	            <input
	                type="file"
	                style={{ display: "none" }}
	                accept="image/x-png,image/jpeg,image/gif"
	                ref={ inputFile }
	                onChange={ handleFileChange } />

	        </form>
            </ModalContent>
        </Modal>
    );
};

export default ChangeProfileImageModal;

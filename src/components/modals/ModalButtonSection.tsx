import React from "react";

interface Props {
  children: JSX.Element | JSX.Element[];
}

const ModalButtonSection: React.FC<Props> = (props: Props) => {

  return(
    <section className="modal__buttonsection">
      { props.children }
    </section>

  );
};

export default ModalButtonSection;

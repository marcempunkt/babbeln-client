import React, { useState, useEffect } from "react";
import logger from "../../utils/logger";
import "./CallModal.scss";

import { ReactComponent as PhoneOff } from "../../assets/feathericons/phone-off.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useCallContext } from "../../contexts/CallContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { AppContextValue,
	 CallContextValue,
	 FriendsContextValue,
	 UserContextValue,
	 ModalContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";

// Cannot find module '*.mp3' or its corresponding type declarations.
// @ts-ignore
import Ringtone from  "../../assets/sounds/chill-abstract-intention-by-coma-media.mp3";

const StartCallModal: React.FC = () => {
  /**
   * CallModal if the client initiates a call
   * @component
   */
  const appContext: AppContextValue = useAppContext();
  const callContext: CallContextValue = useCallContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const userContext: UserContextValue = useUserContext();
  const modalContext: ModalContextValue = useModalContext();

  const [friend, setFriend] = useState<FriendWithAvatar | undefined>(undefined);

  const unmountAfterSecs: number = 84_000;

  useEffect(() => {
    /**
     * get the friend item to display name, avatar, etc.
     * @effect
     * 
     * @dependency  {number}  calling = the id of the user the client is calling 
     */
    const friend: FriendWithAvatar | undefined = friendsContext.getFriend(callContext.calling);
    if (friend) return setFriend(friend);
  }, [callContext.calling]);

  useEffect(() => {
    /**
     * When the StartCallModal mounts start the ringtone
     * @effect
     */
    const ringtone: HTMLAudioElement = new Audio(Ringtone);

    ringtone.loop = true;
    ringtone.volume = 0.5;
    ringtone.load();
    ringtone.play().catch((err: unknown) => console.error(`${err}`));

    return () => {
      ringtone.pause();
      ringtone.currentTime = 0;
      ringtone.srcObject = null;
      ringtone.src = "";
      ringtone.load();
    };
  }, []);

  const handleCancel = () => {
    /**
     * cancel the call & emit it to the calling user
     * @private
     */
    if (!friend) return;
    callContext.endCalling({ fromUser: userContext.loggedInUserId, toUser: friend.friendId }); 
  };

  useEffect(() => {
    /**
     * After x amount of secs unmount the startcall modal
     * by settings the showStartCallModal to false &&
     * resetting the callcontext state
     * @effect
     */
    const timer = setTimeout(() => {
      const isClientInCallingProcess: boolean = (callContext.calling && !callContext.inCallWith) ? true : false;
      logger.log.yellow("StartCallModal", `{ calling: ${callContext.calling}, inCallWith: ${callContext.inCallWith}, isInCall: ${isClientInCallingProcess} }`);
      if (isClientInCallingProcess) {
	modalContext.setShowStartCallModal(false);
	callContext.resetState();
      }
    }, unmountAfterSecs);
    return () => clearTimeout(timer);
  }, []);

  return (
    <div className={ "modal" }>
      <div className={ "callmodal" }>
	<h1 className="modal__title"> { "Calling..." } </h1>

	<img className="callmodal__avatar" src={ (friend) ? friend.avatar : `${appContext.API_URL}/users/get/avatar/0` } />
	<span>{ (friend) ? friend.friendName : "unknown" }</span>

	<section className="callmodal__btns">
	  <button className="btn--error full-width" onClick={ handleCancel }><PhoneOff /></button>
	</section>
      </div>
    </div>
  );
};

export default StartCallModal;

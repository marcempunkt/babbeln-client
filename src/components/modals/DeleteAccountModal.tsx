import React, { useState, FormEvent, ChangeEvent } from "react";
import { useNavigate } from "react-router-dom";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../utils/logger";

import { NotificationType } from "../../ts/types/notification_types";
import { useAppContext } from "../../contexts/AppContext";
import { useSocketContext } from "../../contexts/SocketContext";
import { useUserContext } from "../../contexts/UserContext";
import { useMessageContext } from "../../contexts/MessageContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { usePostContext } from "../../contexts/PostContext";
import { useBlogContext } from "../../contexts/BlogContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useCallContext } from "../../contexts/CallContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { AppContextValue,
	 SocketContextValue,
	 UserContextValue,
	 MessageContextValue,
	 FriendsContextValue,
	 PostContextValue,
	 BlogContextValue,
	 ModalContextValue,
	 CallContextValue,
	 NotificationContextValue } from "../../ts/types/contextvalue_types";
import { Response } from "../../ts/types/axios_types";

import Modal from "./Modal";
import Form from "../form/Form";
import Input from "../form/Input";
import SubmitButton from "../form/SubmitButton";

const DeleteAccountModal: React.FC = () => {

    const correctCheck: string = "Yes, I want to delete my account";

    const navigate = useNavigate();

    const appContext: AppContextValue = useAppContext();
    const socketContext: SocketContextValue = useSocketContext();
    const userContext: UserContextValue = useUserContext();
    const messageContext: MessageContextValue = useMessageContext();
    const friendsContext: FriendsContextValue = useFriendsContext();
    const postContext: PostContextValue = usePostContext();
    const blogContext: BlogContextValue = useBlogContext();
    const modalContext: ModalContextValue = useModalContext();
    const callContext: CallContextValue = useCallContext();
    const notifContext: NotificationContextValue = useNotificationContext();

    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [check, setCheck] = useState<string>("");
    const [error, setError] = useState<string>("");

    const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => setEmail(e.currentTarget.value);
    const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => setPassword(e.currentTarget.value);
    const handleCheckChange = (e: ChangeEvent<HTMLInputElement>) => setCheck(e.currentTarget.value);

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!userContext.token) return logger.error.blue("DeleteAccountModal", "No Authentication Token found.");
        /* Error Handling */
        if (!email || !password || !check) {
            return setError("Please fill out the form.");
        } else if (check !== correctCheck) {
            return setError(`Please enter: "${correctCheck}"`);
        }

        axios.delete(
            `${appContext.API_URL}/users/delete`,
            {
                // TODO test
                data: { email, password },
                headers: { Authorization: `Bearer: ${userContext.token}` }
            }
        ).then((res: AxiosResponse<Response>) => {
	    /* Create notification */
	    notifContext.create(NotificationType.SUCCESS, res.data.message);
	    /* reset all state */
	    appContext.reset();
	    socketContext.reset();
	    userContext.reset();
	    messageContext.reset();
	    friendsContext.reset();
	    postContext.reset();
	    blogContext.reset();
	    modalContext.reset();
	    callContext.reset();
	    /* remove token & go to login page */
	    localStorage.removeItem("auth");
	    navigate("/getting-started");
	}).catch((err: AxiosError<Response>) => {
	    if (err.response) return setError(err.response.data.message);
	});
    };

    const handleCancelClick = () => modalContext.setShowDeleteAccountModal(false);

    return(
        <Modal windowWidth="500px">
            <Form
	        title="Delete my account!"
	        onSubmit={ handleSubmit }
	        error={ error } >

	        <Input
	            htmlId="email"
	            label="E-mail"
	            inputType="email"
	            value={ email }
	            onChange={ handleEmailChange } />

	        <Input
	            htmlId="password"
	            label="Password"
	            inputType="password"
	            value={ password }
	            onChange={ handlePasswordChange } />

	        <Input
	            htmlId="check"
	            label={ "Please enter: " + correctCheck }
	            inputType="text"
	            value={ check }
	            onChange={ handleCheckChange } />

                <button className="btn margin-top" onClick={ handleCancelClick } type="button">Cancel</button>

	        <SubmitButton
	            className="btn--error"
	            isDisabled={ (email && password && check) ? false : true }
	            innerHtml="Delete me!" />
            </Form>
        </Modal>
    );
}

export default DeleteAccountModal;

import React from "react";

interface Props {
  children: string | string[];
}

const ModalTitle: React.FC<Props> = (props: Props) => {
  return(<h1 className="modal__title"> { props.children } </h1>);
};

export default ModalTitle;

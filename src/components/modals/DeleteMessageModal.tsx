import React, { useState, useEffect } from "react";

import { useMessageContext } from "../../contexts/MessageContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useUserContext } from "../../contexts/UserContext";
import { MessageContextValue,
	 NotificationContextValue,
	 ModalContextValue,
	 UserContextValue } from "../../ts/types/contextvalue_types";
import { NotificationType } from "../../ts/types/notification_types";
import { ClientMessage } from "../../ts/types/message_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalContent from "./ModalContent";
import ModalButtonSection from "./ModalButtonSection";
import ModalButton from "./ModalButton";

const DeleteMessageModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();
  const notifContext: NotificationContextValue = useNotificationContext();
  const messageContext: MessageContextValue = useMessageContext();
  const userContext: UserContextValue = useUserContext();

  const [message, setMessage] = useState<ClientMessage | null>(null);

  useEffect(() => {
    // console.log({ deleteMessageId: modalContext.deleteMessageId });
    setMessage(messageContext.getSingleMessage(modalContext.deleteMessageId));
    // console.log(messageContext.getSingleMessage(modalContext.deleteMessageId));
  }, []);

  const handleCancelClick = () => modalContext.setShowDeleteMessageModal(false);

  const handleOnlyForMeClick = () => {
    messageContext.deleteMessageOnlyForMe(modalContext.deleteMessageId);
    modalContext.setShowDeleteMessageModal(false);
  };

  const handleGloballyClick = () => {
    messageContext.deleteMessageGlobally(modalContext.deleteMessageId);
    modalContext.setShowDeleteMessageModal(false);
  };

  return (
    <Modal>
      <ModalTitle>Delete Message?</ModalTitle>

      <ModalContent>{ (message) ? message.content : "unknown" }</ModalContent>

      <ModalButtonSection>
	<ModalButton label="Cancel" onClick={ handleCancelClick } />
	{
	  /* Only render btn for "globally delete a message" if you are the author of that message */
	  (message && message.from_user === userContext.loggedInUserId) ?
	  <React.Fragment>
	    <ModalButton label="Only for me" className="btn--error" onClick={ handleOnlyForMeClick } />
	    <ModalButton label="For everyone" className="btn--error" onClick={ handleGloballyClick } />
	  </React.Fragment>
	  :
	  <React.Fragment>
	    <ModalButton label="Only for me" className="btn--error" onClick={ handleOnlyForMeClick } />
	  </React.Fragment>
	}
      </ModalButtonSection>
    </Modal>
  );
};

export default DeleteMessageModal;

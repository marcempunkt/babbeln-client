import React, { useState, ChangeEvent, FormEvent } from "react";

import { useModalContext } from "../../contexts/ModalContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { ModalContextValue, NotificationContextValue  } from "../../ts/types/contextvalue_types";
import { NotificationType } from "../../ts/types/notification_types";

import Modal from "./Modal";
import Form from "../form/Form";
import Input from "../form/Input";
import SubmitButton from "../form/SubmitButton";
import CloseButton from "../../ui/CloseButton";

const ChangeUsernameModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();
  const notifContext: NotificationContextValue = useNotificationContext();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [error, setError] = useState<string>("");

  const handleUsernameChange = (e: ChangeEvent<HTMLInputElement>) => setUsername(e.currentTarget.value);
  const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => setPassword(e.currentTarget.value);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    /* check if form is filled */
    if (!username || !password) return setError("Please fill out the form");

    // TODO API CALL
    notifContext.create(NotificationType.INFO, "be yet todo :D");
    modalContext.setShowChangeUsernameModal(false);
  };

  const handleCancelClick = () => modalContext.setShowChangeUsernameModal(false);

  return(
    <Modal windowWidth="500px">
      <CloseButton  onClick={ handleCancelClick } />
      <Form
	title="Change my username"
	onSubmit={ handleSubmit }
	error={ error } >

	<Input
	  htmlId="newUsername"
	  label="New username"
	  inputType="text"
	  value={ username }
	  onChange={ handleUsernameChange } />

	<Input
	  htmlId="password"
	  label="Password"
	  inputType="password"
	  value={ password }
	  onChange={ handlePasswordChange } />

	<SubmitButton
	  className="btn--primary"
	  isDisabled={ (username && password) ? false : true }
	  innerHtml="Change username" />

      </Form>
    </Modal>
  );
};

export default ChangeUsernameModal;

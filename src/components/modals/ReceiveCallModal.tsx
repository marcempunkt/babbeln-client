import React, { useState, useEffect } from "react";
import logger from "../../utils/logger";
import "./CallModal.scss";

import { ReactComponent as PhoneCall } from "../../assets/feathericons/phone-call.svg";
import { ReactComponent as PhoneOff } from "../../assets/feathericons/phone-off.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useCallContext } from "../../contexts/CallContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { AppContextValue,
	 CallContextValue,
	 FriendsContextValue,
	 UserContextValue,
	 ModalContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";

// Cannot find module '*.mp3' or its corresponding type declarations.
// @ts-ignore
import Ringtone from  "../../assets/sounds/chill-abstract-intention-by-coma-media.mp3";

const ReceiveCallModal: React.FC = () => {
  /**
   * CallModal if the client receives a call
   * @component
   */
  const appContext: AppContextValue = useAppContext();
  const callContext: CallContextValue = useCallContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const userContext: UserContextValue = useUserContext();
  const modalContext: ModalContextValue = useModalContext();

  /* only a friend can call us! */
  const [friend, setFriend] = useState<FriendWithAvatar | undefined>(undefined);

  const unmountAfterSecs: number = 84_000;

  useEffect(() => {
    /**
     * get the friend item to display name, avatar, etc.
     * @effect
     * 
     * @dependency  {number}  calling = the id of the user calling the client
     */
    const friend: FriendWithAvatar | undefined = friendsContext.getFriend(callContext.calling);
    if (friend) return setFriend(friend);
  }, [callContext.calling]);

  useEffect(() => {
    /**
     * When the StartCallModal mounts start the ringtone
     * @effect
     */
    const ringtone: HTMLAudioElement = new Audio(Ringtone);

    ringtone.loop = true;
    ringtone.volume = 0.5;
    ringtone.load();
    ringtone.play().catch((err: unknown) => console.error(`${err}`)); // FIXME need to await

    return () => {
      ringtone.pause();
      ringtone.currentTime = 0;
      ringtone.srcObject = null;
      ringtone.src = "";
      ringtone.load();
    };
  }, []);

  const handleAccept = () => {
    /**
     * accept the call & start signaling process
     * @private
     */
    if (!friend) return;
    callContext.acceptCalling(friend.friendId);
  }

  const handleCancel = () => {
    /**
     * cancel the call & emit it to the calling user
     * @private
     */
    if (!friend) return;
    callContext.endCalling({ fromUser: userContext.loggedInUserId, toUser: friend.friendId }); 
  };

  useEffect(() => {
    /**
     * After x amount of secs unmount the receive call modal
     * by settings the showReceiveCallModal to false &&
     * resetting the callcontext state
     * @effect
     */
    const timer = setTimeout(() => {
      const isClientInCallingProcess: boolean = (callContext.calling && !callContext.inCallWith) ? true : false;
      logger.log.yellow("ReceiveCallModal", `{ calling: ${callContext.calling}, inCallWith: ${callContext.inCallWith}, isInCall: ${isClientInCallingProcess} }`);
      if (isClientInCallingProcess) {
	modalContext.setShowReceiveCallModal(false);
	callContext.resetState();
      }
    }, unmountAfterSecs);
    return () => clearTimeout(timer);
  }, []);

  return (
    <div className={ "modal" }>
      <div className={ "callmodal" }>
	<h1 className="modal__title"> { "Incoming call..." } </h1>

	<img className="callmodal__avatar" src={ (friend) ? friend.avatar : `${appContext.API_URL}/users/get/avatar/0` } />
	<span>{ (friend) ? friend.friendName : "unknown" }</span>

	<section className="callmodal__btns">
	  <button className="btn--error full-width margin-right" onClick={ handleCancel }><PhoneOff /></button>
	  <button className="btn--success full-width" onClick={ handleAccept }><PhoneCall /></button>
	</section>
      </div>
    </div>
  );
};

export default ReceiveCallModal;

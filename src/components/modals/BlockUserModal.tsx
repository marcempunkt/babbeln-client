import React, { useState, useEffect } from "react";
import axios from "axios";

import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { UserContextValue, ModalContextValue, FriendsContextValue } from "../../ts/types/contextvalue_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalButtonSection from "./ModalButtonSection";
import ModalButton from "./ModalButton";

const BlockUserModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();
  const userContext: UserContextValue = useUserContext();
  const friendsContext: FriendsContextValue = useFriendsContext();

  const [username, setUsername] = useState<string>("");

  const handleCancelClick = () => modalContext.setShowBlockUserModal(false);

  const handleBlock = () => {
    friendsContext.blockFriend(modalContext.blockOrUnblockUserId);
    modalContext.setShowBlockUserModal(false);
  };

  useEffect(() => {
    /**
     * 
     * @effect
     */
    /* KeyListener for ENTER */
    const windowKeyHandler = (e: KeyboardEvent) => {
      if (e.key === "Enter") return handleBlock();
    };

    window.addEventListener("keydown", windowKeyHandler);

    return () => window.removeEventListener("keydown", windowKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    /* get username by showProfileId */
    userContext.getUsernameById(modalContext.blockOrUnblockUserId, source.token).then(username => {
      setUsername(username);
    }).catch((_err) => {});

    return () => source.cancel()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalContext.blockOrUnblockUserId]);

  return (
    <Modal>
      <ModalTitle>Block { username }?</ModalTitle>
      
      <ModalButtonSection>
	<ModalButton label="No" onClick={ handleCancelClick } />
	<ModalButton label="Yes!" className="btn--primary" onClick={ handleBlock } />
      </ModalButtonSection>
    </Modal>
  );
};

export default BlockUserModal;

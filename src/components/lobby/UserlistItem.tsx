import React, { MouseEvent } from "react";

import { ReactComponent as User } from "../../assets/feathericons/user.svg";
import { ReactComponent as UserMinus } from "../../assets/feathericons/user-minus.svg";
import { ReactComponent as UserX } from "../../assets/feathericons/user-x.svg";
import { ReactComponent as X } from "../../assets/feathericons/x.svg";
import { ReactComponent as Trash } from "../../assets/feathericons/trash-2.svg";

import { useMessageContext } from "../../contexts/MessageContext";
import { useAppContext } from "../../contexts/AppContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useModalContext } from "../../contexts/ModalContext";
import { MessageContextValue,
         AppContextValue,
         FriendsContextValue,
         ContextmenuContextValue,
         ModalContextValue } from "../../ts/types/contextvalue_types";
import { Friend } from "../../ts/types/friends_types";

import ContextmenuItem from "../contextmenu/ContextmenuItem";
import GenericUserlistItem from "../GenericUserlistItem";

interface Props {
    userId: number;
    isActive: boolean;
}

const UserlistItem: React.FC<Props> = (props: Props) => {
    /**
     * Listitem for UserList
     * @component
     */
    const messageContext: MessageContextValue = useMessageContext();
    const appContext: AppContextValue = useAppContext();
    const friendsContext: FriendsContextValue = useFriendsContext();
    const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
    const modalContext: ModalContextValue = useModalContext();

    const handleClick = () => {
        messageContext.setShowMessagesFromUserId(props.userId);
        appContext.setShowFriends(false);
        appContext.setShowDashboard(false);
        appContext.setShowTimeline(false);
    };

    const handleBlock = () => {
        modalContext.setBlockOrUnblockUserId(props.userId);
        modalContext.setShowBlockUserModal(true);
    };

    const handleUnblock = () => {
        modalContext.setBlockOrUnblockUserId(props.userId);
        modalContext.setShowUnblockUserModal(true);
    }

    const handleDeleteChat = (e: MouseEvent<SVGElement> | undefined) => {
        if (e) {
            e.stopPropagation();
        }
        modalContext.setShowDeleteChatModal(true);
        modalContext.setDeleteChatFromUserId(props.userId);
    };

    const showProfileMenuItem = <ContextmenuItem
                                    key={ 1 }
                                    icon={ <User /> }
                                    description="Show Profile"
                                    handleClick={ () => contextmenuContext.handleShowProfile(props.userId) }/>;

    const removeFriendMenuItem = <ContextmenuItem
                                     key={ 2 }
                                     icon={ <UserMinus /> }
                                     description="Remove Friend"
				     handleClick={ () => contextmenuContext.handleRemoveFriend(props.userId) }/>;

    const blockFriendMenuItem = <ContextmenuItem
                                    key={ 3 }
                                    icon={ <UserX /> }
                                    description="Block"
                                    handleClick={ handleBlock }/>;

    const unblockFriendMenuItem = <ContextmenuItem
                                      key={ 4 }
                                      icon={ <UserX /> }
                                      description="Unblock"
                                      handleClick={ handleUnblock }/>;

    const deleteChatMenuItem = <ContextmenuItem
                                   key={ 5 }
                                   icon={ <Trash /> }
                                   description="Delete Chat"
                                   handleClick={ () => handleDeleteChat(undefined) } />;

    const handleContextmenu = (e: MouseEvent<HTMLLIElement>) => {
        e.preventDefault();

        /* Spawn Contextmenu */
        contextmenuContext.create(e.pageX, e.pageY);

        /* Check if they're friends */
        const currentFriend: Friend | undefined = friendsContext.getFriend(props.userId);

        /* aren't friends */
        if (!currentFriend) return contextmenuContext.setComponents([deleteChatMenuItem]);

        /* is blocked */
        if (currentFriend.blocked) return contextmenuContext.setComponents([
            showProfileMenuItem,
            deleteChatMenuItem,
            removeFriendMenuItem,
            unblockFriendMenuItem
        ]);

        /* default => are friends & isn't blocked */
        return contextmenuContext.setComponents([
            showProfileMenuItem,
            deleteChatMenuItem,
            removeFriendMenuItem,
            blockFriendMenuItem
        ]);
    };

    return(
        <GenericUserlistItem
            className={ ((props.isActive) ? "useritem--active" : "") }
            userId={ props.userId }
            onContextmenu={ handleContextmenu }
            onClick={ handleClick }
            actions={ [<X key={ 1 } className="useritem__actions__red" onClick={ handleDeleteChat } />] } />
    );
};

export default UserlistItem;

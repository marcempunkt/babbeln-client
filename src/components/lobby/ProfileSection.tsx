import React, { useState } from "react";
import "./ProfileSection.scss";

import { ReactComponent as LogOut } from "../../assets/feathericons/log-out.svg";
import { ReactComponent as Settings } from "../../assets/feathericons/settings.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { AppContextValue, UserContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

import Tooltip, { TooltipPlacement } from "../Tooltip";
import ChangeStatusDialog from "./changestatus/ChangeStatusDialog";

const ProfileSection: React.FC = () => {
  /**
   * ProfileSection inside Lobby Component
   * @component
   */
  const appContext: AppContextValue = useAppContext();
  const userContext: UserContextValue = useUserContext();
  const modalContext: ModalContextValue = useModalContext();

  const [showChangeStatusDialog, setShowChangeStatusDialog] = useState<boolean>(false);

  const handleImageClick = () => setShowChangeStatusDialog(!showChangeStatusDialog); 

  const handleSettingsClick = () => appContext.setShowSettings(true);

  const handleLogoutClick = () => modalContext.setShowLogoutModal(true);

  return(
    <div className="lobby__profilesection">

      <div className="lobby__profilesection__profile">
        <img
          className={"profilesection__userimage" + userContext.handleStatusClasses(userContext.status)}
          onClick={ handleImageClick }
          src={ userContext.avatar }
          alt="my avatar"/>
        <span className="profilesection__username">{ userContext.loggedInUsername }</span>

	{ (showChangeStatusDialog) ? <ChangeStatusDialog onClose={ () => setShowChangeStatusDialog(false) } /> : null }
      </div>

      <div className="lobby__profilesection__btns">
        <Tooltip title="Logout" placement={ TooltipPlacement.BOTTOM }>
          <LogOut className="lobby__profilesection__btns__logout" onClick={ handleLogoutClick }/>
        </Tooltip>

        <Tooltip title="Settings" placement={ TooltipPlacement.BOTTOM }>
          <Settings className="lobby__profilesection__btns__settings" onClick={ handleSettingsClick} />
        </Tooltip>
      </div>

    </div>
  );
}

export default ProfileSection;

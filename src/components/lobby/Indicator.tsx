import react from "react";
import "./Indicator.scss";

interface Props {
  number: number;
};

const Indicator: React.FC<Props> = (props: Props) => {

  return(
    <div className="indicator">
      { props.number }
    </div>
  );
};

export default Indicator;

import React from "react";
import "./DirectMessagesUserlist.scss";

import DirectMessagesUserlistItem from "./DirectMessagesUserlistItem";

interface Props {
    directMessages: Array<number>;
}

const DirectMessagesUserlist: React.FC<Props> = (props: Props) => {

    return(
        <UserList>
            <ul className="directmessagesuserlist">
                {
                    props.directMessages.map((userId: number) => (
                        <DirectMessagesUserlistItem key={ userId } userId={ userId } />   
                    ))
                }
            </ul>
        </UserList>
    );
};

export default DirectMessagesUserlist;

import React from "react";

interface Props {
  children: React.ReactChild[];
}

const Userlist: React.FC<Props> = (props: Props) => {
  /**
   * Wrapper for UserlistItems
   * @component
   */
  return(
    <ul className="userlist">
      { props.children }
    </ul>
  );
}

export default Userlist;

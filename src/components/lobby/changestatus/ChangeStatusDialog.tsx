import React, { useEffect } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../../utils/logger";
import "./ChangeStatusDialog.scss";

import { useAppContext } from "../../../contexts/AppContext";
import { useUserContext } from "../../../contexts/UserContext";
import { useNotificationContext } from "../../../contexts/NotificationContext";
import type { AppContextValue,
	      UserContextValue,
	      NotificationContextValue } from "../../../ts/types/contextvalue_types";
import { NotificationType } from "../../../ts/types/notification_types";
import { Status } from "../../../ts/types/friends_types";
import type { Response } from "../../../ts/types/axios_types";
import OnlineCircle from "./OnlineCircle";
import OfflineCircle from "./OfflineCircle";
import AwayCircle from "./AwayCircle";
import DndCircle from "./DndCircle";

interface Props {
    onClose: () => void;
}

const ChangeStatusDialog: React.FC<Props> = (props: Props) => {

    const appContext: AppContextValue = useAppContext();
    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();

    const handleStatusChange: (newStatus: Status) => void = (newStatus) => {
        /**
         * update your online status inside the client's state and then emit a socket event
         * @private
         */
        if (!userContext.token) return logger.error.blue("ChangeStatusDialog", "No Authentication Token found.");

        axios.patch(
            `${appContext.API_URL}/users/change/status`,
            { newStatus },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((_res: AxiosResponse<Status>) => {
            userContext.updateStatus(newStatus);
            props.onClose();
        }).catch((err: AxiosError<Response>) => {
            console.error(err);
            notifContext.create(NotificationType.ERROR, "Couldn't update your status");
            props.onClose();
        });
    };

    useEffect(() => {
        /* IMPORTANT: 
         * In case of using this with React make sure you don't accidentally use import { KeyboardEvent } from "react";.
         * This leads to the not assignable to type 'EventListener' exception. (Credit: stackoverflow manu) */
        const changestatusdialogKeyHandler = (e: KeyboardEvent) => {
            if (e.key === "Escape") return props.onClose()
        };

        const handleDocumentClick = () => {
            props.onClose(); 
        }

        window.addEventListener("keydown", changestatusdialogKeyHandler);
        // TODO add click to disable changestatusdialog
        // document.addEventListener("click", handleDocumentClick);

        return () => {
            window.removeEventListener("keydown", changestatusdialogKeyHandler); 
            // document.removeEventListener("click", handleDocumentClick);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return(
        <div className="changestatusdialog">
            <ul className="changestatusdialog__list">
	        <li className="changestatusdialog__list__item" onClick={ () => handleStatusChange(Status.ON) }><OnlineCircle />Online</li>
	        <li className="changestatusdialog__list__item" onClick={ () => handleStatusChange(Status.AWAY) }><AwayCircle />Away</li>
	        <li className="changestatusdialog__list__item" onClick={ () => handleStatusChange(Status.DND) }><DndCircle />Do not disturb</li>
	        <li className="changestatusdialog__list__item" onClick={ () => handleStatusChange(Status.OFF) }><OfflineCircle />Invisible</li>
            </ul>
        </div>
    );
};

export default ChangeStatusDialog;

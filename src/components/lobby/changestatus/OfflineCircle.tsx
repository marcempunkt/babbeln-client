import React from "react";

const OfflineCircle: React.FC = () => {

  return(
    <div className="changestatusdialog__offline"></div>
  );
};

export default OfflineCircle;

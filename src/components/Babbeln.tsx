import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../utils/logger";
import "./Babbeln.scss";

import { Status } from "../ts/types/user_types";
import { Response } from "../ts/types/axios_types";
import { AppContextValue,
	 UserContextValue,
	 ContextmenuContextValue,
	 ModalContextValue,
	 SocketContextValue,
	 MessageContextValue,
	 FriendsContextValue,
	 FriendrequestsContextValue,
	 PostContextValue,
	 CallContextValue } from "../ts/types/contextvalue_types";
import { useUserContext } from "../contexts/UserContext";
import { useAppContext } from "../contexts/AppContext";
import { useModalContext } from "../contexts/ModalContext";
import { useContextmenuContext } from "../contexts/ContextmenuContext";
import { useSocketContext } from "../contexts/SocketContext";
import { useMessageContext } from "../contexts/MessageContext";
import { useFriendsContext } from "../contexts/FriendsContext";
import { useFriendrequestsContext } from "../contexts/FriendrequestsContext";
import { usePostContext } from "../contexts/PostContext";
import { useCallContext } from "../contexts/CallContext";

import Lobby from "./lobby/Lobby";
import MainWindow from "./MainWindow";
import Settings from "./settings/Settings";
import Contextmenu from "./contextmenu/Contextmenu";
import ConnectionLostSplashscreen from "./ConnectionLostSplashscreen";
import SearchMessages from "./chatcontainer/SearchMessages";

const Babbeln: React.FC = () => {
    /**
     * The Dashboard Component does:
     *   1. Checks if authentication token
     *   3. Handles which Modal should be printed
     *   4. Renders Settings Component
     *   5. Renders ContextMenu
     *   6. Renders Lobby and MainWindow
     * @component
     */
    const userContext: UserContextValue = useUserContext();
    const appContext: AppContextValue = useAppContext();
    const modalContext: ModalContextValue = useModalContext();
    const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
    const socketContext: SocketContextValue = useSocketContext();
    const messageContext: MessageContextValue = useMessageContext();
    const friendsContext: FriendsContextValue = useFriendsContext();
    const friendrequestsContext: FriendrequestsContextValue = useFriendrequestsContext();
    const postContext: PostContextValue = usePostContext();
    const callContext: CallContextValue = useCallContext();

    const navigate = useNavigate();

    const [showConnectionLostSplashscreen, setShowConnectionLostSplashscreen] = useState<boolean>(false);

    const renderSettings: () => JSX.Element | undefined = () => {
        /*
         * renders Settings Component if appContext.showSettings is true
         * @private
         *
         * returns {React.FC}     Settings
         */
        if (appContext.showSettings) return <Settings />;
    };

    const renderConnectionLostSplashscreen: () => JSX.Element | undefined = () => {
        if (showConnectionLostSplashscreen) return <ConnectionLostSplashscreen />;
    };

    const renderSearchMessages: () => undefined | JSX.Element = () => {
        /**
         * Retrun the 
         */
        if (messageContext.showSearchMessages) return <SearchMessages />;
        return;
    };

    const listenOnConnected = (socketid: string) => {
        /**
         * listener for the socket event 'connected'
         * refetch messages, friends, friendrequests, posts, user information because of laptop or smartphone or lost connection
         * @private
         */
        if (socketContext.reconnected) {
            logger.log.blue("Babbeln", "Reconnected to Socket Server.")
            /* refetch all the state:
             *  User
             *  Friends
             *  Friendrequests
             *  Messages
             *  Posts
             */
            userContext.fetchUser();
            friendsContext.fetchFriends();
            friendrequestsContext.fetchPendings();
            messageContext.fetchMessages();
            postContext.fetchPosts();
        } else {
            logger.log.blue("Babbeln", "Connected to Socket Server")
        }

        socketContext.setReconnected(new Date().valueOf());
        callContext.setLocalSocketid(socketid);
        callContext.callingReconnected(socketid);

        /* Emit update_status here because on other parts of the code the socket could be null
         * But here it is the old status instead of the new one */
        // HTTP GET status of this user...
        if (socketContext.socket && userContext.loggedInUserId) {
            axios.get(`${appContext.API_URL}/users/get/status/${userContext.loggedInUserId}`)
	         .then((res: AxiosResponse<Status>) => {
	             socketContext.socket?.emit("update_status", userContext.loggedInUserId, res.data);
	         })
	         .catch((err: AxiosError<Response>) => {
	             console.error(err);
	             if (err.response) return console.error(err.response.data.message);
	         });
        }
    };

    const validateToken = () => {
        /**
         * If there isn't a token send the client to the login screen
         * because no token means no user is currently logged in!
         * @public
         */
        const token: null | string = localStorage.getItem("auth");
        if (!token) return navigate("/getting-started");

        axios.post(`${appContext.API_URL}/users/validate-token`, { token })
	     .then((res: AxiosResponse<boolean>) => {
	         setShowConnectionLostSplashscreen(false);
	         const isValid: boolean = res.data;
	         if (isValid) {
	             return userContext.setToken(token); 
	         } else {
	             navigate("/getting-started");
	             localStorage.removeItem("auth");
	             return logger.log.blue("Babbeln", "validate-token response is false");
	         }
	     })
	     .catch((err: AxiosError<boolean>) => {
	         switch (err.message) {
	             case "Network Error": {
	                 setShowConnectionLostSplashscreen(true);
	                 setTimeout(() => validateToken(), 10_000);
	                 return logger.log.blue("Babbeln", "Network Error validating token");
	             }
	             default: {
	                 // - db was removed/resetted and therefore the jwt is unvalid
	                 // - if jwt is unvalid kick the client to the login screen (HTTP ERR RESPONSE 401)
	                 // - the jwt should also be unvalid when the password changed
	                 // disable splash screen!
	                 setShowConnectionLostSplashscreen(false);
	                 localStorage.removeItem("auth");
	                 navigate("/getting-started");
	             }
	         }
	     });
    };

    /**
     * On mount validate the authentication token (json-web-token)
     * @effect
     */
    useEffect(() => { validateToken(); }, []);

    useEffect(() => {
        /**
         * Socket EventListeners
         * "mounts" all event listerners for socket.io
         * emit update_status event when successfully connected to the socket server
         * this is one of the only ways to be sure to be 100% sure that after you logged in
         * all of your friends will get the socket event to see that you are in fact online atm
         * @effect
         */
        if (socketContext.socket) {
            socketContext.socket.on(
	        "connected",
	        (socketid: string) => listenOnConnected(socketid)
            );
        }
        return () => {
            if (socketContext.socket) {
	        socketContext.socket.off("connected");
            }
        };
    }, [socketContext.socket,
        socketContext.reconnected,
        userContext.loggedInUserId,
        userContext.status,
        userContext.token,
        callContext.inCallWith,
        callContext.localSocketid,
        callContext.remoteSocketid]);

    return(
        <div className="babbeln">
            { modalContext.renderModal() }
            { modalContext.renderCallModal() }
            { renderSearchMessages() }
            { renderSettings() }
            { renderConnectionLostSplashscreen() }
            <Lobby />
            <MainWindow />
            { (contextmenuContext.showContextmenu) ? <Contextmenu /> : null }
        </div>
    );
}

export default Babbeln;

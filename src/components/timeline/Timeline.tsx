import React from "react";
import "./Timeline.scss";

import { ReactComponent as Feather } from "../../assets/feathericons/feather.svg";
import { ReactComponent as Plus } from "../../assets/feathericons/plus.svg";

import { usePostContext } from "../../contexts/PostContext";
import { PostContextValue } from "../../ts/types/contextvalue_types";
import { Post as PostType } from "../../ts/types/post_types";
import Post from "./Post";
import CreatePost from "./CreatePost";

const Timeline: React.FC = () => {

  const postContext: PostContextValue = usePostContext();

  const handleCreateNewPostClick = () => postContext.setShowCreatePost(true);

  return(
    <div className="timeline">
      <header className="timeline__header">
	<div className="timeline__header__content">
	  <h1 className="timeline__header__title margin-right"><Feather />Timeline</h1>
          <button className="btn--success btn--with-svg" onClick={ handleCreateNewPostClick } ><Plus />Create&nbsp;new&nbsp;post</button>
	</div>
	<hr />
      </header>

      <main className="timeline__content">
	{
	  (postContext.showCreatePost) ?
	  <CreatePost />
	  :
	  postContext.posts
		     .sort((x: PostType, y: PostType) => y.writtenAt - x.writtenAt)
		     .map((post: PostType) => (
	    <Post
	      key={ post.id }
	      postId={ post.id }
	      writtenBy={ post.writtenBy }
	      content={ post.content }
	      images={ post.images }
	      likes={ post.likes }
	      comments={ post.comments }
	      writtenAt={ post.writtenAt } />
	  ))
	}
      </main>
    </div>
  );
};

export default Timeline;

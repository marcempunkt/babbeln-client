import React, { useState, useEffect } from "react";
import "./Post.scss";

import { useFriendsContext } from "../../contexts/FriendsContext";
import { useUserContext } from "../../contexts/UserContext";
import { FriendsContextValue, UserContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";
import { TinyInt } from "../../ts/types/mariadb_types";
import { PostComment } from "../../ts/types/post_types";

import PostHeader from "./PostHeader";
import PostContent from "./PostContent";
import PostLikes from "./PostLikes";
import PostComments from "./PostComments";
import PostCommentfield from "./PostCommentfield";
import PostMeatballMenu from "./PostMeatballMenu";

interface Props {
  postId: number;
  writtenBy: number;
  content: string;
  images: Array<string>;
  likes: Array<number>;
  comments: Array<PostComment>
  writtenAt: number;
}

const Post: React.FC<Props> = (props: Props) => {

  const friendsContext: FriendsContextValue = useFriendsContext();
  const userContext: UserContextValue = useUserContext();

  const [friend, setFriend] = useState<FriendWithAvatar | undefined>(undefined);

  useEffect(() => {
    /**
     * Get friend (-object) for each post
     * @effect
     */
    setFriend(friendsContext.getFriend(props.writtenBy));
  }, [props, friendsContext.friends]);


  /* Don't render any posts that aren't my friends and aren't written by me */
  if (friend && friend.blocked === TinyInt.TRUE) return null;
  if (!friend && props.writtenBy !== userContext.loggedInUserId) return null;


  return(
    <div className="post">

      { /* only render meatballmenu if you're the author of the post */
	(props.writtenBy === userContext.loggedInUserId) ? <PostMeatballMenu postId={ props.postId } /> : null
      }

      <PostHeader friend={ friend } writtenBy={ props.writtenBy } writtenAt={ props.writtenAt } />
      <PostContent images={ props.images } content={ props.content } />
      <PostLikes postId={ props.postId } likes={ props.likes } />
      { /* only render comments if there are comments */
	(props.comments.length) ? <PostComments comments={ props.comments } postId={ props.postId } /> : null
      }

      <PostCommentfield postId={ props.postId } />
      <hr />

    </div>
  );
};

export default Post;

import React, { useState, useEffect } from "react";
import "./PostContent.scss";

import { useAppContext } from "../../contexts/AppContext";
import { AppContextValue } from "../../ts/types/contextvalue_types";
import PostGallery from "./PostGallery";

interface Props {
  images: Array<string>;
  content: string;
}

const PostContent: React.FC<Props> = (props: Props) => {

  const appContext: AppContextValue = useAppContext();

  const [content, setContent] = useState<Array<string | JSX.Element>>([]);

  useEffect(() => {
    /**
     * linkify conent of a post
     * @effect
     */
    const linkifiedContent: Array<string | JSX.Element> = appContext.linkify(props.content);
    setContent(linkifiedContent);
  }, [props.content]);

  return(
    <main className="post__content">
      <PostGallery images={ props.images } />
      <pre className="post__content__text">
	{ ...content }
      </pre>
    </main>
  );
};

export default PostContent;

import React, { MouseEvent } from "react";

import { useModalContext } from "../../contexts/ModalContext";
import { usePostContext } from "../../contexts/PostContext";
import { ModalContextValue, PostContextValue } from "../../ts/types/contextvalue_types";

import CloseButton from "../../ui/CloseButton";

interface Props {
  images: Array<string>;
}

const CreatePostGallery: React.FC<Props> = (props: Props) => {

  const modalContext: ModalContextValue = useModalContext();
  const postContext: PostContextValue = usePostContext();

  const handleImageClick: (imgSrc: string) => void = (imgSrc) => {
    modalContext.setImageModalSrc(imgSrc);
    modalContext.setShowImageModal(true);
  };

  const handleRemoveImage = (e: MouseEvent<HTMLDivElement>, imageUrl: string) => {
    e.stopPropagation();
    postContext.setNewPostImages(
      postContext.newPostImages.filter((previewImg: string) => previewImg !== imageUrl)
    );
  };

  /* if there are none images it won't be rendered at all */
  if (!props.images.length) return null
  return(
    <div className="post__content__gallery">
      {
	props.images.map((image: string) => {
	  return(
	    <div
	      key={ image }
	      onClick={ () => handleImageClick(image) }
	      className="post__content__gallery__img" style={ { backgroundImage: `url(${image})` } }>
	      <CloseButton onClick={ (e: MouseEvent<HTMLDivElement>) => handleRemoveImage(e, image) } />
	    </div>
	  );
	})
      }
    </div>
  );
};

export default CreatePostGallery;

import React, { useState, useEffect } from "react";
import "./PostMeatballMenu.scss";

import { ReactComponent as Trash } from "../../assets/feathericons/trash-2.svg";
import { ReactComponent as MoreVertical } from "../../assets/feathericons/more-vertical.svg";

import { usePostContext } from "../../contexts/PostContext";
import { PostContextValue } from "../../ts/types/contextvalue_types";

interface Props {
  postId: number;
}

const PostMeatballMenu: React.FC<Props> = (props: Props) => {

  const postContext: PostContextValue = usePostContext();

  const [show, setShow] = useState<boolean>(false);

  const handleMeatballClick = () => setShow(!show);

  const close = () => setShow(false);

  useEffect(() => {
    /* IMPORTANT: 
     * In case of using this with React make sure you don't accidentally use import { KeyboardEvent } from "react";.
     * This leads to the not assignable to type 'EventListener' exception. (Credit: stackoverflow manu) 
     */
    const meatballmenuKeyHandler = (e: KeyboardEvent) => { if (e.key === "Escape") return close() };

    document.addEventListener("keydown", meatballmenuKeyHandler);

    return () => {
      document.removeEventListener("keydown", meatballmenuKeyHandler); 
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  return(
       <div className="post__meatball" onClick={ handleMeatballClick } >
	<MoreVertical className="meatball-svg" />
	{
	  (show) ?
	  <ul className="post__meatball__menu" >
	    <li className="post__meatball__menu__item post__meatball__menu__item--red"
		onClick={ () => postContext.handleDelete(props.postId) }
		onMouseOut={ close } >
	      <Trash />Delete&nbsp;post
	    </li>
	  </ul>
	  :
	  null
	}
      </div>
   
  );
};

export default PostMeatballMenu;

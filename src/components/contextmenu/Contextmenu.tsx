import React, { useEffect } from "react";
import "./Contextmenu.scss";

import { ReactComponent as X } from "../../assets/feathericons/x.svg";

import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { ContextmenuContextValue } from "../../ts/types/contextvalue_types";

import ContextmenuItem from "./ContextmenuItem";


const Contextmenu: React.FC = () => {

  const contextmenuContext: ContextmenuContextValue = useContextmenuContext();

  const handleClose = () => contextmenuContext.setShowContextmenu(false);

  useEffect(() => {
    /**
     * Add Document(Global)-wide EventListener for unmounting Contextmenu on rightclick or <Escape>
     * @effect
     */
    const contextmenuKeyHandler = (e: KeyboardEvent) => { if (e.key === "Escape") return handleClose() };
    document.addEventListener("keydown", contextmenuKeyHandler);
    document.addEventListener("click", handleClose);
    return(() => {
      document.removeEventListener("keydown", contextmenuKeyHandler); 
      document.removeEventListener("click", handleClose);
    });
  }, []);

  return(
    <div className="contextmenu" style={{
      top: `${ contextmenuContext.posY }px`,
      left: `${ contextmenuContext.posX }px`,
    }}>
      { contextmenuContext.components.map(component => component) }
      <ContextmenuItem icon={ <X /> } description="Close" handleClick={ handleClose }/>
    </div>
  );
}

export default Contextmenu;

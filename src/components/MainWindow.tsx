import React from "react";

import { MessageContextValue, AppContextValue } from "../ts/types/contextvalue_types";

import { useMessageContext } from "../contexts/MessageContext";
import { useAppContext } from "../contexts/AppContext";

import ChatContainer from "./chatcontainer/ChatContainer";
import Friends from "./friends/Friends";
import Dashboard from "./dashboard/Dashboard";
import Timeline from "./timeline/Timeline";

const MainWindow: React.FC = () => {

  const messageContext: MessageContextValue = useMessageContext();
  const appContext: AppContextValue = useAppContext();

  const renderComponent: () => JSX.Element | null = () => {
    /*
     * Renders Content inside MainWindow
     * @private
     *
     * returns {React.FC}     Content for MainWindow
     */
    if (appContext.showDashboard) {
      return <Dashboard />
    } else if (appContext.showFriends) {
      return <Friends />
    } else if (appContext.showTimeline) {
      return <Timeline />
    } else if (messageContext.showMessagesFromUserId) {
      return <ChatContainer />
    } else {
      return null
    }
  };

  return(
    <React.Fragment>
      { renderComponent() }
    </React.Fragment>
  );
}

export default MainWindow;

import React from "react";
import "./PremiumPreview.scss";

import { useModalContext } from "../../contexts/ModalContext";
import { ModalContextValue } from "../../ts/types/contextvalue_types";

const PremiumPreview: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();

  const handleClick: () => void = () => modalContext.setShowPremiumModal(true);

  return(
    <div className="premiumpreview margin-bottom" onClick={ handleClick }>
      <span className="premiumpreview__innerText">Become a Babbler!</span>
    </div>
  );
};

export default PremiumPreview;

import React from "react";
import "./BlogPreview.scss";

import { useModalContext } from "../../contexts/ModalContext";
import { useBlogContext } from "../../contexts/BlogContext";
import { ModalContextValue, BlogContextValue } from "../../ts/types/contextvalue_types";

const BlogPreview: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();
  const blogContext: BlogContextValue = useBlogContext();

  const handleClick: () => void = () => {
    return modalContext.setShowBlogModal(true); // TODO remove this statement when blogmodal(-preview) is finished
    /* if (blogContext.blogs.length) {
     *   modalContext.setShowBlogModal(true);
     * } */
  };

  return(
    <div className="blogpreview" onClick={ handleClick }>
      {
	(blogContext.blogs.length) ? <p>{ "Somehow true" }</p> : <p>{ "There are no News" }</p>
      }
    </div>
  );
};

export default BlogPreview;

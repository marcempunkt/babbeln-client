import React from "react";
import "./Dashboard.scss";

import { ReactComponent as DashboardIcon } from "../../assets/feathericons/layers.svg";

import PremiumPreview from "./PremiumPreview";
import TimelinePreview from "./TimelinePreview";
import BlogPreview from "./BlogPreview";

const Dashboard: React.FC = () => {

  return(
    <div className="dashboard">

      <header className="dashboard__header">
	<h1 className="dashboard__header__title"><DashboardIcon />Dashboard</h1>
	<hr />
      </header>

      <main className="dashboard__content">
	<div className="dashboard__content__hybaspace">
	  <PremiumPreview />
	</div>

	<div className="dashboard__content__timeline-and-blog">
	  <TimelinePreview />
	  <BlogPreview />
	</div>
      </main>
      
    </div>
  );
};

export default Dashboard;

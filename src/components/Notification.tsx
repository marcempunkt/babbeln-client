import React, { useEffect } from "react";
import "./Notification.scss";

import { ReactComponent as AlertCircle } from "../assets/feathericons/alert-circle.svg";
import { ReactComponent as Info } from "../assets/feathericons/info.svg";
import { ReactComponent as CheckCircle } from "../assets/feathericons/check-circle.svg";

import { NotificationContextValue } from "../ts/types/contextvalue_types";
import { NotificationType } from "../ts/types/notification_types";
import { useNotificationContext } from "../contexts/NotificationContext";

const Notification: React.FC = () => {
  /**
   * Component for rendering the whole /getting-started page
   * @component
   */

  const notifContext: NotificationContextValue = useNotificationContext();

  useEffect(() => {
    /**
     * Validates the token and if token is valid send him to Dashboard /"root"
     *
     * @effect
     * @dependencies  {history}  history
     */
    if (notifContext.showMessage) {
      notifContext.timeout.create();
    }
  }, [notifContext]);

  const handleClose = () => {
    /**
     * reducer function for messages.
     * @private
     *
     * @param  {MessageState}  state          all messages sortet after two Arrays (SEND_MESSAGES & RECEIVED_MESSAGES)
     * @param  {any}           action         action object for reducer function
     *
     * returns {MessageState}  state that has been alternated by the action
     */
    notifContext.timeout.cancel();
    notifContext.setShowMessage(false);
  };

  const renderNotifIcon: () => JSX.Element = () => {
    /**
     * reducer function for messages.
     * @private
     *
     * @param  {MessageState}  state          all messages sortet after two Arrays (SEND_MESSAGES & RECEIVED_MESSAGES)
     * @param  {any}           action         action object for reducer function
     *
     * returns {MessageState}  state that has been alternated by the action
     */
    switch (notifContext.notificationType) {
      case NotificationType.SUCCESS:
        return <CheckCircle className="success-svg" />;
      case NotificationType.ERROR:
        return <AlertCircle className="error-svg" />;
      case NotificationType.INFO:
        return <Info className="info-svg"/>;
      default:
        return <Info className="info-svg"/>;
    }
  };

  const getClasses: () => string = () => {
    /**
     * reducer function for messages.
     * @private
     *
     * @param  {MessageState}  state          all messages sortet after two Arrays (SEND_MESSAGES & RECEIVED_MESSAGES)
     * @param  {any}           action         action object for reducer function
     *
     * returns {MessageState}  state that has been alternated by the action
     */
    switch (notifContext.notificationType) {
      case NotificationType.SUCCESS:
        return "notifi--success";
      case NotificationType.ERROR:
        return "notifi--error";
      case NotificationType.INFO:
        return "notifi";
      default:
        return "notifi";
    }
  }

  return(
    (notifContext.showMessage) ?
    <div
      className={ getClasses() }
      onClick={ handleClose }>
      { renderNotifIcon() }
      { notifContext.message }
    </div> : null
  );
}

export default Notification;

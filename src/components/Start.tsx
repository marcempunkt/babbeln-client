import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios, { AxiosRequestConfig } from "axios";
import "./Start.scss";

import { useSocketContext } from "../contexts/SocketContext";
import { useAppContext } from "../contexts/AppContext";
import { SocketContextValue, AppContextValue } from "../ts/types/contextvalue_types";

import Signup from "./Signup";
import Login from "./Login";

const Start: React.FC = () => {
  /**
   * Component for rendering the whole /getting-started page
   * @component
   */
  const socketContext: SocketContextValue = useSocketContext();
  const appContext: AppContextValue = useAppContext();
  const navigate = useNavigate();

  const [showLogin, setShowLogin] = useState<boolean>(true);

  const handleChangeView = () => (setShowLogin(!showLogin));

  useEffect(() => {
    /**
     * Validates the token and if token is valid send him to Dashboard /"root"
     * @effect
     * @dependencies  {history}  history
     */
    const token: string | null = localStorage.getItem("auth");
    if (token) { navigate("/") };
  }, []);

  useEffect(() => {
    /**
     * Disconnect from Socket Server after componentDidMount
     * @effect
     */
    socketContext.manuallyDisconnect();
  }, []);

  return(
    <div className="start dark">
      {
	(showLogin) ?
	<Login onChangeView={ handleChangeView }/>
	:
	<Signup onChangeView={ handleChangeView }/>
      }
    </div>
  );
}

export default Start;

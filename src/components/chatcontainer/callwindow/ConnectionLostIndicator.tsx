import React from "react";
import "./ConnectionLostIndicator.scss";

import { ReactComponent as WifiOff } from "../../../assets/feathericons/wifi-off.svg";

const ConnectionLostIndicator: React.FC = () => {

  return(
    <div className="connectionlostindicator" title="No connection, may impact your call...">
      <WifiOff className="connectionlostindicator__icon" />
    </div>
  );
};

export default ConnectionLostIndicator;

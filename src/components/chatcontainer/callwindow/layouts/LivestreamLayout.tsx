import React, { useMemo, useState, useRef, useEffect } from "react";
import "./LivestreamLayout.scss"; 
import { ReactComponent as Minimize } from "../../../../assets/feathericons/minimize-2.svg";
import { ReactComponent as Maximize } from "../../../../assets/feathericons/maximize-2.svg";

import { useCallContext } from "../../../../contexts/CallContext";
import { CallContextValue } from "../../../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../../../ts/types/friends_types";

import LivestreamImageBackground from "../../../../assets/images/livestream_background.jpg";
import ScreenshareVideo from "../ScreenshareVideo";
import WebcamVideo from "../WebcamVideo";

interface Props {
  participants: Array<FriendWithAvatar>;
}

const LivestreamLayout: React.FC<Props> = (props: Props) => {

  const callContext: CallContextValue = useCallContext();

  const [showWebcam, setShowWebcam] = useState<boolean>(true);
  const [livestreamlayoutHeight, setLivestreamlayoutHeight] = useState<number>(0);
  const livestreamlayoutRef = useRef<HTMLDivElement | null>(null);

  const toggleWebcam = () => setShowWebcam(!showWebcam);

  useEffect(() => {
    const resizeObserver: ResizeObserver = new ResizeObserver((entries: Array<ResizeObserverEntry>) => {
      // console.log({ clientHeight: entries[0].target.clientHeight })
      setLivestreamlayoutHeight(entries[0].target.clientHeight);
    });

    if (livestreamlayoutRef?.current) {
      resizeObserver.observe(livestreamlayoutRef.current)
    }

    return () => {
      if (livestreamlayoutRef?.current) { resizeObserver.unobserve(livestreamlayoutRef.current) };
    }
  }, []);

  return(
    <div className="livestreamlayout" ref={ livestreamlayoutRef }>
      {
	(callContext.remoteScreenshareStream) ?
	<React.Fragment>
	  <div className="livestreamlayout__stream">
	    <ScreenshareVideo stream={ callContext.remoteScreenshareStream } />
	  </div>

	  <div className={
	  "livestreamlayout__speaker" + ((showWebcam) ? "" : "--minimized") + ((livestreamlayoutHeight < 315) ? " livestreamlayout__speaker--hide" : "") }>
	    <div className="livestreamlayout__speaker__badge" onClick={ toggleWebcam }>
	      <span className="livestreamlayout__speaker__badge__name">{ props.participants[0].friendName }</span>
	      {
		(showWebcam) ?
		<Minimize className="livestreamlayout__speaker__badge__minmax" />
		:
		<Maximize className="livestreamlayout__speaker__badge__minmax" />
	      }
	    </div>
	    {
	      (showWebcam) ?
	      <WebcamVideo stream={ callContext.remoteStream } avatar={ props.participants[0].avatar } />
	      :
	      null
	    }
	  </div>
	</React.Fragment>
	:
	<div className="livestreamlayout__speaker--full">
	  <WebcamVideo stream={ callContext.remoteStream } avatar={ props.participants[0].avatar } />
	</div>
      }
    </div>
  );
};

export default LivestreamLayout;

import React, { MouseEvent, MouseEventHandler } from "react";
import "./MosaicTile.scss";

interface Props {
  children: JSX.Element;
  nametag?: string;
  onContextMenu?: (e: MouseEvent<HTMLDivElement>) => void;
  onClick?: MouseEventHandler<HTMLDivElement>;
  className?: string;
}

const MosaicTile: React.FC<Props> = (props: Props) => {

  return(
    <div
      className={ "mosaictile" + ((props.className) ? (" " + props.className) : "") }
      onContextMenu={ props.onContextMenu }
      onClick={ (props.onClick) ? props.onClick : undefined }>
      { props.children }
      { (props.nametag) ? <span className="mosaictile__nametag">{ props.nametag }</span> : null }
    </div>
  );
};

export default MosaicTile;

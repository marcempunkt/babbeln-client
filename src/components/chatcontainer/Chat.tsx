import React, { useState, useEffect, useRef } from "react";
import "./Chat.scss";

import { useMessageContext } from "../../contexts/MessageContext";
import { useUserContext } from "../../contexts/UserContext";
import { MessageContextValue, UserContextValue } from "../../ts/types/contextvalue_types";
import { ClientMessage } from "../../ts/types/message_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";

import Message from "./Message";
import DeletedMessage from "./DeletedMessage";

interface Props {
  friend: FriendWithAvatar | undefined;
}

const Chat: React.FC<Props> = (props: Props) => {
  /**
   * Renders all Messages of a Conversation
   * @component
   */
  const messageContext: MessageContextValue = useMessageContext();
  const userContext: UserContextValue = useUserContext();

  const chatRef = useRef<HTMLDivElement | null>(null);

  const [chats, setChats] = useState<Array<ClientMessage>>([]);

  let start: number = 0;
  let hasToday: boolean = false;

  const renderDivider = (sendAt: number) => {
    /**
     * renderDivider function:
     * if the actual Date of the message is greater than date
     * => return a divider
     * => set date to the date in millisec of the message
     * if the date of the message equals today and we yet havn't rendered a "today" divider
     * => render a "today" divider
     * => set hasToday to true so that we don't render it more than once
     */
    const sendAtDate: string = new Date(sendAt).toLocaleDateString("de-De");
    const previousDate: string = new Date(start).toLocaleDateString("de-De")
    const today: string = new Date().toLocaleDateString("de-De");
    /* return ---12.06.1997--- */
    if (sendAtDate > previousDate && sendAtDate !== today) {
      /* set the millisec of the date of the message to the initial date */
      start = sendAt;
      return(
        <div className="messages__divider">
          <span>{ new Date(sendAt).toLocaleDateString("de-De") }</span>
        </div>
      );
      /* return ---Today--- */
    } else if (sendAtDate === today && !hasToday) {
      start = sendAt;
      /* because no we'll return a today divider */
      /* we set hasToday to true so that this if statement doesn't get called again */
      hasToday = true;
      return(
        <div className="messages__divider">
          <span>Today</span>
        </div>
      );
    }
  };

  const shouldMessageBeGrouped: (thisMessage: ClientMessage, index: number) => boolean = (thisMessage, index) => {
    const nextMessage: ClientMessage | undefined = chats[index + 1];
    if (!nextMessage) return false;
    const thisMessageDate: string = new Date(thisMessage.send_at).toLocaleDateString("de-De");
    const nextMessageDate: string = new Date(nextMessage.send_at).toLocaleDateString("de-De");
    if (thisMessageDate !== nextMessageDate) return false;
    return thisMessage.from_user === nextMessage.from_user;
  };

  useEffect(() => {
    /**
     * Filters messages to show only messages of a conversation and sorts them
     *
     * @dependency  {Array<ClientMessage>}  messageContext.messages 
     * @dependency  {number}                userContext.loggedInUserId
     * @dependency  {number}                messageContext.showMessageFromUserId
     * 
     * @effect
     */
    const allMessages: Array<ClientMessage> = messageContext.messages.filter((msg: ClientMessage) => {
      const iSent:      boolean = msg.from_user === userContext.loggedInUserId && msg.to_user === messageContext.showMessagesFromUserId;
      const friendSent: boolean = msg.from_user === messageContext.showMessagesFromUserId && msg.to_user === userContext.loggedInUserId;
      return (iSent || friendSent);
    })
    allMessages.sort((x: ClientMessage, y: ClientMessage) => x.send_at - y.send_at);
    setChats(allMessages);
  }, [messageContext.messages, userContext.loggedInUserId, messageContext.showMessagesFromUserId]);

  useEffect(() => {
    /**
     * scroll to bottom every time messages change
     * @effect
     */
    // Wait 1ms because React is weird and the height of <chat> changes
    // so at the time this effect gets called, the div is 1/3 of the size it actually should be
    // but weirdly the chat div height property is accurate even though the scrollTop = scrollHeight
    // does not work...
    setTimeout(() => chatRef?.current?.scrollTo(0, chatRef.current.scrollHeight), 10);
  }, [chats]);

  return(
    <div className="chat" ref={ chatRef }>
      {
	chats.map((msg: ClientMessage, index: number) => (
          <React.Fragment key={ msg.id }>
            { renderDivider(msg.send_at) }

            {
	      (msg.is_deleted) ?
              <DeletedMessage sendAt={ msg.send_at } key={ msg.id } />
              :
              <Message
		friend={ props.friend }
		msgId={ msg.id }
		fromUser={ msg.from_user }
		msgContent={ msg.content }
		sendAt={ msg.send_at }
	        grouped={ shouldMessageBeGrouped(msg, index) }
		key={ msg.id }
	      />
	    }
          </React.Fragment>
	))
      }
    </div>
  );
};

export default Chat;

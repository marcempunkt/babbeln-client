import React from "react";
import "./DeletedMessage.scss";

import { useAppContext } from "../../contexts/AppContext";
import { AppContextValue } from "../../ts/types/contextvalue_types";

interface Props {
  sendAt: number;
}

const DeletedMessage: React.FC<Props> = (props: Props) => {
  /**
   * Renders a deleted message
   * @component
   */
  const appContext: AppContextValue = useAppContext();

  return(
      <div
        className="deleted">
        <span className="message__content">{ appContext.handleDate(props.sendAt) }: This messages has been deleted.</span>
      </div>
  );
};

export default DeletedMessage;

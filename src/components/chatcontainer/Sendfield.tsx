import React, { useState, useEffect, useRef, FormEvent, ChangeEvent } from "react";
import logger from "../../utils/logger";
import "./Sendfield.scss";

import { ReactComponent as Send } from "../../assets/feathericons/send.svg";

import { useMessageContext } from "../../contexts/MessageContext";
import { useUserContext } from "../../contexts/UserContext";
import { useSocketContext } from "../../contexts/SocketContext";
import { MessageContextValue,
	 UserContextValue,
	 SocketContextValue } from "../../ts/types/contextvalue_types";
import { SendMessage, Draft } from "../../ts/types/message_types";
import { Friend } from "../../ts/types/friends_types";
import { TinyInt } from "../../ts/types/mariadb_types";
import { SocketState } from "../../ts/types/socket_types";

interface Props {
  friend: Friend | undefined;
}

const Sendfield: React.FC<Props> = (props: Props) => {

  const userContext: UserContextValue = useUserContext();
  const messageContext: MessageContextValue = useMessageContext();
  const socketContext: SocketContextValue = useSocketContext();

  const [message, setMessage] = useState<string>("");
  const [submitting, setSubmitting] = useState<boolean>(false);
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(false);
  const [textareaDisabled, setTextareaDisabled] = useState<boolean>(false)

  const formRef = useRef<HTMLFormElement>(null);

  /* Reset message, because Sendfield won't unmount when opening another chat */
  useEffect(() => { setMessage("") }, [messageContext.showMessagesFromUserId]);

  useEffect(() => {
    /**
     * Update the state of the submit button & textarea
     * enable or disable it
     * @effect
     */
    // const isMessageEmpty: boolean = (message.length === 0) ? true : false;
    const isNotFriend: boolean = (props.friend) ? false : true;
    const isBlocked: boolean = (props.friend && props.friend.blocked) ? true : false;
    
    /* Update Button disabled attribute */
    if (isNotFriend || isBlocked || submitting) {
      setButtonDisabled(true);
    } else {
      setButtonDisabled(false);
    }

    /* Update textarea disabled attribute */
    if (isNotFriend || isBlocked || submitting) {
      setTextareaDisabled(true);
    } else {
      setTextareaDisabled(false);
    }

  }, [socketContext.state, props.friend, message, submitting]);

  const handleChangeMessage = (event: ChangeEvent<HTMLTextAreaElement>) => setMessage(event.currentTarget.value);

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    /**
     * Submit-Handler-Function for this component
     * if there is no connection to the socket server go into a recursive-loop
     * and retry every 0.5s
     * @private
     */
    event.preventDefault();

    /* Prevent sending empty messages */
    if (!message) return logger.error.blue("Sendfield", "Please enter a message first."); 

    /* Enter submitting mode */
    setSubmitting(true);

    /* wait if the client isn't connected to the socket server */
    if (socketContext.state === SocketState.Disconnected) {
      setTimeout(() => { if (formRef.current) { formRef.current.requestSubmit(); } }, 500);
      return logger.error.blue("Sendfield", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    /* Send message */
    const msg: SendMessage = {
      from_user: userContext.loggedInUserId,
      to_user: messageContext.showMessagesFromUserId,
      content: message,
    };

    messageContext.sendMessage(msg);
    setSubmitting(false);
    setMessage("");
    /* Reset draft 
     * messageContext.drafts.map((draft: Draft, index: number) => {
     *   if (draft.userId === messageContext.showMessagesFromUserId) {
     *     let copiedDrafts: Array<Draft> = [...messageContext.drafts];
     *     copiedDrafts[index].content = "";
     *     return messageContext.setDrafts(copiedDrafts);
     *   }
     * }); */
  };

  const handlePlaceholder: () => string = () => {
    /**
     * Returns the correct string as a placeholder for a textarea of a chat
     * @private
     *
     * returns  {string}  placeholder
     */
    if (!props.friend) return `You aren't friends with this user anymore`;
    if (props.friend.blocked === TinyInt.TRUE) return `You've blocked ${props.friend.friendName}!`;
    return `Message @${props.friend.friendName}`;
  };

  const keyHandler = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    /**
     * Eventlistener for textarea
     * On 'Enter' submit the form
     * On 'Shift + Enter' insert breakline
     */
    const sendfield: HTMLFormElement = document.querySelector("#sendfield")!;

    switch(e.key) {
      case "Enter":
	if (!e.shiftKey) {
	  e.preventDefault();
	  if (message) return sendfield.requestSubmit();
	}
    }
  };

  /*   FIXME: Save Drafts!!!
   *   useEffect(() => {
   *     let noDraftFound: boolean = true;
   *     // Check if there is already a draft object for this chat
   *     messageContext.drafts.forEach((draft: Draft, index: number) => {
   *       if (draft.userId === messageContext.showMessagesFromUserId) {
   * 	noDraftFound = false;
   * 	// update its content
   * 	let copiedDrafts: Array<Draft> = [...messageContext.drafts];
   * 	copiedDrafts[index].content = message;
   * 	messageContext.setDrafts(copiedDrafts);
   *       }
   *     });
   *     // create new draft
   *     if (noDraftFound) {
   *       messageContext.setDrafts(messageContext.drafts.concat([{
   * 	userId: messageContext.showMessagesFromUserId,
   * 	content: message
   *       }]));
   *     }
   *   }, [messageContext.showMessagesFromUserId])
   * 
   *   useEffect(() => {
   *     let isDraft = false;
   *     messageContext.drafts.map((draft: Draft) => {
   *       if (draft.userId === messageContext.showMessagesFromUserId && draft.content !== "") {
   * 	isDraft = true;
   * 	return setMessage(draft.content);
   *       }
   *     });
   *   }, []); */

  return(
    <form id="sendfield" className="sendfield" onSubmit={ handleSubmit } ref={ formRef }>

      <textarea
        className="sendfield__textarea"
        placeholder={ handlePlaceholder() }
        onChange={ handleChangeMessage }
        onKeyDown={ keyHandler }
        value={ message }
	disabled={ textareaDisabled } />

      <button
	type="submit"
	className="sendfield__sendbutton"
	disabled={ buttonDisabled }>
	<Send />
      </button>
    </form>
  );
};

export default Sendfield;

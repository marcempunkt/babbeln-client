import React from "react";
import "./CloseButton.scss";
import { ReactComponent as X } from "../assets/feathericons/x.svg";

type AnyFunction = any;

interface Props {
    onClick: AnyFunction;
    position?: "left" | "right";
    className?: string;
}

const CloseButton: React.FC<Props> = (props: Props) => {

    const styles = {
        left: (props.position === "left" ? "1rem" : "auto"),
        right: (props.position === "right" ? "1rem" : "auto"),
    };

    return(
        <X
            onClick={ props.onClick }
            className={"closebutton " + ((props.className) ? props.className : "") }
            style={ styles }
        />
    );
};

CloseButton.defaultProps = {
    position: "left",
};

export default CloseButton;

import { Dispatch, SetStateAction } from "react";
import { Socket } from "socket.io-client";
import { CancelToken, Response } from "./axios_types";
import { ServerToClientEvents, ClientToServerEvents, SocketState } from "./socket_types";
import { MessageState, ClientMessage, SendMessage, ReducerAction, Draft } from "./message_types";
import { TinyInt } from "./mariadb_types";
import { NotificationType } from "./notification_types";
import { Friend,
	 FriendWithAvatar,
	 Friendrequest,
	 FriendsReducerAction,
	 PendingsReducerAction } from "./friends_types";
import { Post, PostsReducerAction } from "./post_types";
import { Theme } from "./config_types";
import { Blog } from "./blog_types";
import { Status } from "./user_types";
import { Layout, CallingType } from "./call_types";

export interface AppContextValue {
    API_URL: string;
    /* State */
    timezone: string;
    setTimezone: Dispatch<SetStateAction<string>>;
    theme: Theme;
    setTheme: Dispatch<SetStateAction<Theme>>;
    showSettings: boolean;
    setShowSettings: Dispatch<SetStateAction<boolean>>;
    showFriends: boolean;
    setShowFriends: Dispatch<SetStateAction<boolean>>;
    showDashboard: boolean;
    setShowDashboard: Dispatch<SetStateAction<boolean>>;
    showTimeline: boolean;
    setShowTimeline: Dispatch<SetStateAction<boolean>>;
    /* Methods */
    reset: () => void;
    handleDate: (milisec: number) => string;
    getFileUrls: (fileList: FileList) => Array<Promise<string>>;
    isElectron: () => boolean;
    linkify: (content: string) => Array<string | JSX.Element>;
};

export interface SocketContextValue {
    /* ~State~ */
    socket: Socket<ServerToClientEvents, ClientToServerEvents> | undefined;
    setSocket: Dispatch<SetStateAction<Socket<ServerToClientEvents, ClientToServerEvents> | undefined>>;
    state: SocketState;
    setState: Dispatch<SetStateAction<SocketState>>;
    reconnected: number;
    setReconnected: Dispatch<SetStateAction<number>>;
    /* ~Methods~ */
    reset: () => void;
    createSocketConnection: (auth: string) => boolean;
    manuallyDisconnect: () => boolean; 
    maybeReconnect: () => boolean;
};

export interface UserContextValue {
    /* ~State~ */
    token: string | null;
    setToken: Dispatch<SetStateAction<string | null>>;
    loggedInUserId: number;
    setLoggedInUserId: Dispatch<SetStateAction<number>>;
    loggedInUsername: string;
    setLoggedInUsername: Dispatch<SetStateAction<string>>;
    status: Status | null;
    setStatus: Dispatch<SetStateAction<Status | null>>;
    registerDate: number;
    setRegisterDate: Dispatch<SetStateAction<number>>;
    isActive: TinyInt;
    setIsActive: Dispatch<SetStateAction<TinyInt>>;
    email: string;
    setEmail: Dispatch<SetStateAction<string>>;
    avatar: string;
    setAvatar: Dispatch<SetStateAction<string>>;
    /* ~Methods~ */
    reset: () => void;
    fetchUser: () => void;
    getUsernameById: (userId: number, cancelToken: CancelToken) => Promise<string>;
    getRegisterDateById: (userId: number, cancelToken: CancelToken) => Promise<number>;
    updateAvatar: () => void;
    updateUsername: (newUsername: string) => void;
    updateStatus: (status: Status) => void;
    handleStatusClasses: (onlineStatus: Status | null) => string;
};

export interface MessageContextValue {
    /* ~State~ */
    messages: MessageState;
    dispatchMessages: Dispatch<ReducerAction>;
    showMessagesFromUserId: number;
    setShowMessagesFromUserId: Dispatch<SetStateAction<number>>;
    drafts: Array<Draft>;
    setDrafts: Dispatch<SetStateAction<Array<Draft>>>;
    showSearchMessages: boolean;
    setShowSearchMessages: Dispatch<SetStateAction<boolean>>;
    /* ~Methods~ */
    reset: () => void;
    fetchMessages: () => void;
    sendMessage: (msg: SendMessage) => void;
    deleteMessageGlobally: (msgId: number) => void;
    deleteMessageOnlyForMe: (msgId: number) => void;
    deleteChat: (deleteChatWithUser: number) => void;
    getSingleMessage: (msgId: number) => ClientMessage | null;
};

export interface CallContextValue {
    /* ~State~ */
    peerConnection: RTCPeerConnection | null;
    setPeerConnection: Dispatch<SetStateAction<RTCPeerConnection | null>>
    localSocketid: string | undefined;
    setLocalSocketid: Dispatch<SetStateAction<string | undefined>>;
    remoteSocketid: string | undefined;
    setRemoteSocketid: Dispatch<SetStateAction<string | undefined>>;
    callingType: CallingType;
    setCallingType: Dispatch<SetStateAction<CallingType>>;
    calling: number;
    setCalling: Dispatch<SetStateAction<number>>;
    inCallWith: number;
    setInCallWith: Dispatch<SetStateAction<number>>;
    callWindowLayout: Layout;
    setCallWindowLayout: Dispatch<SetStateAction<Layout>>;
    /* locale audio, video & screen */
    mic: boolean;
    setMic: Dispatch<SetStateAction<boolean>>;
    showCam: boolean;
    setShowCam: Dispatch<SetStateAction<boolean>>;
    showScreen: boolean;
    setShowScreen: Dispatch<SetStateAction<boolean>>;
    /* remote audio, video & screen */
    remoteMic: boolean;
    setRemoteMic: Dispatch<SetStateAction<boolean>>;
    remoteShowCam: boolean;
    setRemoteShowCam: Dispatch<SetStateAction<boolean>>;
    remoteShowScreen: boolean;
    setRemoteShowScreen: Dispatch<SetStateAction<boolean>>;
    /* Streams */
    localStream: MediaStream | null;
    setLocalStream: Dispatch<SetStateAction<MediaStream | null>>;
    remoteStream: MediaStream | null;
    setRemoteStream: Dispatch<SetStateAction<MediaStream | null>>;
    remoteStreamVolume: number;
    setRemoteStreamVolume: Dispatch<SetStateAction<number>>;
    screensharePeerConnection: RTCPeerConnection | null;
    setScreensharePeerConnection: Dispatch<SetStateAction<RTCPeerConnection | null>>;
    localScreenshareStream: MediaStream | null;
    setLocalScreenshareStream: Dispatch<SetStateAction<MediaStream | null>>;
    remoteScreenshareStream: MediaStream | null;
    setRemoteScreenshareStream: Dispatch<SetStateAction<MediaStream | null>>;
    remoteStreamAudio: HTMLAudioElement | null;
    setRemoteStreamAudio: Dispatch<SetStateAction<HTMLAudioElement | null>>;
    remoteStreamAudioContext: AudioContext | null;
    setRemoteStreamAudioContext: Dispatch<SetStateAction<AudioContext | null>>;
    remoteStreamGainNode: GainNode | null;
    setRemoteStreamGainNode: Dispatch<SetStateAction<GainNode | null>>;
    /* ~Methods~ */
    resetState: () => void;
    reset: () => void;
    startCalling: (withUser: number, callingType?: CallingType) => void;
    endCalling: (data: { fromUser: number; toUser: number; toSocketid?: string }) => void;
    acceptCalling: (withUser: number) => void;
    signalingStartCall: (data: { fromUser: number; toUser: number; fromSocketid: string; toSocketid: string }) => void;
    startScreenshareSignaling: (stream: MediaStream) => void;
    startScreenshareRenegotiation: () => void;
    screenshareClose: () => void;
    hangup: (event: "emit" | "on") => void;
    startRenegotiation: () => void;
    callingReconnected: (socketid: string) => void;
};


export interface ContextmenuContextValue {
    /* State */
    posX: number;
    setPosX: Dispatch<SetStateAction<number>>;
    posY: number;
    setPosY: Dispatch<SetStateAction<number>>;
    showContextmenu: boolean;
    setShowContextmenu: Dispatch<SetStateAction<boolean>>;
    components: Array<JSX.Element>;
    setComponents: Dispatch<SetStateAction<Array<JSX.Element>>>;
    /* Methods */
    create: (x: number, y: number) => void;
    handleRemoveFriend: (friendId: number) => void;
    handleShowProfile: (userId: number) => void;
    handleStartChat: (userId: number) => void;
};

export interface FriendsContextValue {
    /* State */
    friends: Array<FriendWithAvatar>;
    dispatchFriends: Dispatch<FriendsReducerAction>;
    removeFriendId: number;
    setRemoveFriendId: Dispatch<SetStateAction<number>>;
    showOnline: boolean;
    setShowOnline: Dispatch<SetStateAction<boolean>>;
    showAll: boolean;
    setShowAll: Dispatch<SetStateAction<boolean>>;
    showPending: boolean;
    setShowPending: Dispatch<SetStateAction<boolean>>;
    showBlocked: boolean;
    setShowBlocked: Dispatch<SetStateAction<boolean>>;
    showAddFriend: boolean;
    setShowAddFriend: Dispatch<SetStateAction<boolean>>;
    /* Methods */
    reset: () => void;
    fetchFriends: () => void;
    removeFriend: (friendId: number) => void;
    blockFriend: (friendId: number) => void;
    unblockFriend: (friendId: number) => void;
    getFriend: (friendId: number) => FriendWithAvatar | undefined;
    getAvatar: (friend: FriendWithAvatar | undefined) => string;
};

export interface FriendrequestsContextValue {
    /* State */
    pendings: Array<Friendrequest>;
    dispatchPendings: Dispatch<PendingsReducerAction>;
    /* Methods */
    reset: () => void;
    fetchPendings: () => void;
    acceptFriendrequest: (pendingId: number) => void; 
    declineFriendrequest: (friendreqId: number) => void;
    sendFriendrequest: (friendId: number) => void;
};


export interface NotificationContextValue {
    /* State */
    message: string;
    setMessage: Dispatch<SetStateAction<string>>;
    notificationType: NotificationType;
    setNotificationType: Dispatch<SetStateAction<NotificationType>>; 
    showMessage: boolean;
    setShowMessage: Dispatch<SetStateAction<boolean>>;
    /* Methods */
    timeout: {
        create: () => void;
        cancel: () => void;
    };
    create: (notifType: NotificationType, msg: string) => void;
    playSoundReceivedMessage: () => void;
    playSoundSendMessage: () => void;
    playSoundFriendrequest: () => void;
    playSoundStartCall: () => void;
    playSoundEndCall: () => void;
    playSoundStartScreenshare: () => void;
    playSoundCloseScreenshare: () => void;
};

export interface ModalContextValue {
    /* State */
    showReportMessageModal: boolean;
    setShowReportMessageModal: Dispatch<SetStateAction<boolean>>;
    reportMessageId: number;
    setReportMessageId: Dispatch<SetStateAction<number>>;
    showDeleteMessageModal: boolean;
    setShowDeleteMessageModal: Dispatch<SetStateAction<boolean>>;
    deleteMessageId: number;
    setDeleteMessageId: Dispatch<SetStateAction<number>>;
    showDeleteChatModal: boolean;
    setShowDeleteChatModal: Dispatch<SetStateAction<boolean>>;
    deleteChatFromUserId: number;
    setDeleteChatFromUserId: Dispatch<SetStateAction<number>>;

    showChangeProfileImageModal: boolean;
    setShowChangeProfileImageModal: Dispatch<SetStateAction<boolean>>;
    showChangeUsernameModal: boolean;
    setShowChangeUsernameModal: Dispatch<SetStateAction<boolean>>;
    showChangeEmailModal: boolean;
    setShowChangeEmailModal: Dispatch<SetStateAction<boolean>>;
    showChangePasswordModal: boolean;
    setShowChangePasswordModal: Dispatch<SetStateAction<boolean>>;
    showDeleteAccountModal: boolean;
    setShowDeleteAccountModal: Dispatch<SetStateAction<boolean>>;

    showProfileModal: boolean;
    setShowProfileModal: Dispatch<SetStateAction<boolean>>;
    showProfileId: number;
    setShowProfileId: Dispatch<SetStateAction<number>>;

    showRemoveFriendModal: boolean;
    setShowRemoveFriendModal: Dispatch<SetStateAction<boolean>>;

    showLogoutModal: boolean;
    setShowLogoutModal: Dispatch<SetStateAction<boolean>>;

    blockOrUnblockUserId: number;
    setBlockOrUnblockUserId: Dispatch<SetStateAction<number>>;
    showBlockUserModal: boolean;
    setShowBlockUserModal: Dispatch<SetStateAction<boolean>>;
    showUnblockUserModal: boolean;
    setShowUnblockUserModal: Dispatch<SetStateAction<boolean>>;

    showBlogModal: boolean;
    setShowBlogModal: Dispatch<SetStateAction<boolean>>;
    showPremiumModal: boolean;
    setShowPremiumModal: Dispatch<SetStateAction<boolean>>;

    showImageModal: boolean;
    setShowImageModal: Dispatch<SetStateAction<boolean>>;
    imageModalSrc: string;
    setImageModalSrc: Dispatch<SetStateAction<string>>;

    showStartCallModal: boolean;
    setShowStartCallModal: Dispatch<SetStateAction<boolean>>;
    showReceiveCallModal: boolean;
    setShowReceiveCallModal: Dispatch<SetStateAction<boolean>>;
    showScreenshareModal: boolean;
    setShowScreenshareModal: Dispatch<SetStateAction<boolean>>;

    /* Methods */
    reset: () => void;
    renderModal: () => JSX.Element | null;
    renderCallModal: () => JSX.Element | null;
};

export interface PostContextValue {
    /* State */
    posts: Array<Post>;
    dispatchPosts: Dispatch<PostsReducerAction>;
    showCreatePost: boolean;
    setShowCreatePost: Dispatch<SetStateAction<boolean>>;
    newPostContent: string;
    setNewPostContent: Dispatch<SetStateAction<string>>;
    newPostImages: Array<string>;
    setNewPostImages: Dispatch<SetStateAction<Array<string>>>;
    reset: () => void;
    fetchPosts: () => void;
    handleLike: (profileId: number, userId: number) => void;
    handleDelete: (postId: number) => void;
    handleDate: (writtenAt: number) => string;
};

export interface BlogContextValue {
    blogs: Array<Blog>;
    setBlogs: Dispatch<SetStateAction<Array<Blog>>>;
    reset: () => void;
}

export enum Theme {
  LIGHT = "light",
  DARK = "dark",
  NORD_LIGHT = "nord-light",
  NORD_DARK = "nord-dark",
}

export enum Layout {
  Mosaic = "mosaic",
  Livestream = "livestream",
}

export enum CallingType {
  Audio = "audio",
  Video = "video",
}

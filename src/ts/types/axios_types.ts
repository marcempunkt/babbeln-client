export interface Response {
    message: string; 
}

export type CancelToken = any;

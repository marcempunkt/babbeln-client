import { TinyInt } from "./mariadb_types";

export enum Status {
  ON = "on",
  OFF = "off",
  DND = "dnd",
  AWAY = "away",
}

export interface User {
  id: number;
  username: string;
  email: string;
  password: string;
  is_active: TinyInt;
  socketid: string;
  status: Status;
  register_date: number;
}

export interface UserWithoutPassword {
  id: number;
  email: string;
  username: string;
  is_active: TinyInt;
  socketid: string;
  status: Status;
  register_date: number;
}

export interface FuzzyUser {
    id: number;
    username: string;
}


export interface Post {
  id: number;
  writtenBy: number;
  content: string;
  images: Array<string>;
  likes: Array<number>;
  comments: Array<PostComment>;
  writtenAt: number;
}

export interface PostComment {
  commentId: number;
  userId: number;
  content: string;
  writtenAt: number;
}

export enum PostsReducerActionType {
  FETCH,
  REMOVE,
  ADD,
  LIKE_OR_DISLIKE,
  REMOVE_COMMENT,
  ADD_COMMENT,
  LOGOUT,
}

export interface PostsReducerAction {
  type: PostsReducerActionType;
  payload?: any;
}


type LoggerFn = (header: string, message: string) => void;

const log  = (args: Array<string>) => console.log(...args);
const err  = (args: Array<string>) => console.error(...args);
const warn = (args: Array<string>) => console.warn(...args);

const styles = {

  normal: [
    "font-weight: bold",
  ].join(";"),

  blue: [
    "color: #2669d4",
    "font-weight: bold",
  ].join(";"),

  magenta: [
    "color: #9e41ee",
    "font-weight: bold",
  ].join(";"),

  yellow: [
    "color: #ffcd3a",
    "font-weight: bold",
  ].join(";"),

};

const normal        : LoggerFn = (header, message) => log([`%c[${header}]`, styles.normal, `${message}`]);
const blue          : LoggerFn = (header, message) => log([`%c[${header}]`, styles.blue, `${message}`]);
const magenta       : LoggerFn = (header, message) => log([`%c[${header}]`, styles.magenta, `${message}`]);
const yellow        : LoggerFn = (header, message) => log([`%c[${header}]`, styles.yellow, `${message}`]);

const normalError   : LoggerFn = (header, message) => err([`%c[${header}]`, styles.normal, `${message}`]);
const blueError     : LoggerFn = (header, message) => err([`%c[${header}]`, styles.blue, `${message}`]);
const magentaError  : LoggerFn = (header, message) => err([`%c[${header}]`, styles.magenta, `${message}`]);
const yellowError   : LoggerFn = (header, message) => err([`%c[${header}]`, styles.yellow, `${message}`]);

const normalWarning : LoggerFn = (header, message) => warn([`%c[${header}]`, styles.normal, `${message}`]);
const blueWarning   : LoggerFn = (header, message) => warn([`%c[${header}]`, styles.blue, `${message}`]);
const magentaWarning: LoggerFn = (header, message) => warn([`%c[${header}]`, styles.magenta, `${message}`]);
const yellowWarning : LoggerFn = (header, message) => warn([`%c[${header}]`, styles.yellow, `${message}`]);

const logger = {
  log: {
    normal: normal,
    blue: blue,
    magenta: magenta,
    yellow: yellow,
  },
  error: {
    normal: normalError,
    blue: blueError,
    magenta: magentaError,
    yellow: yellowError,
  },
  warning: {
    normal: normalWarning,
    blue: blueWarning,
    magenta: magentaWarning,
    yellow: yellowWarning,
  }
};

export default logger;

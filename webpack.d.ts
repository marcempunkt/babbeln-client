// SVG type
declare module '*.svg' {
  import React = require('react');
  export const ReactComponent: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  const src: string;
  export default src;
}
// PNG type
declare module "*.png" {
  const value: any;
  export default value;
}
// JPG type
declare module "*.jpg" {
  const value: any;
  export default value;
}
// MP3 type
declare module "*.mp3" {
    const value: string;
    export default value;
}

	
